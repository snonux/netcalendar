/* NetCalendar 2006, 2009 (c) Dipl.-Inform. (FH) Paul C. Buetow
 * http://buetow.org - netcalendar@dev.buetow.org
 */

/**
 *
 */
package server;

import java.io.*;
import java.net.*;
import java.util.*;
import javax.net.*;
import javax.net.ssl.*;

import shared.*;
import shared.remotecall.*;

/**
 * This is the main class of the server part of the netcalendar suite.
 * It contains the server socket part and hold a references to the calendar database.
 * @author Paul C. Buetow
 */
public class NetCalendarServer extends Thread {
    private int iPort;
    private String sWorkdir;

    /**
     * Creates a NetCalendarServer instance and runs it as a thread.
     * @param iPort Specifies the server port of the server to be used.
     * @param sWorkdir Specifies the working directory of the server.
     */
    public NetCalendarServer(int iPort, String sWorkdir) {
        this.iPort = iPort;
        this.sWorkdir = sWorkdir;

        start();
    }

    /**
     * This method initializes a new server socket.
     * @return Returns the server socket object.
     */
    private ServerSocket makeServerSocket() throws IOException {
        if (!Config.getBooleanValue("use_ssl"))
            return new ServerSocket(iPort);

        ServerSocketFactory sslSocketFactory = SSLServerSocketFactory.getDefault();
        return sslSocketFactory.createServerSocket(iPort);
    }

    /**
     * This method specifies the start method of the Thread. Its setting up the server port.
     */
    public void run() {
        ServerSocket serverSocket = null;
        Socket socket = null;

        // Read and parse the whole calendar database from file!
        CalendarFormatParser parser = new CalendarFormatParser();
        parser.setWorkdir(sWorkdir);
        parser.start();

        CalendarDatabase calendarDatabase = new CalendarDatabase(parser.getCategories());

        try {
            serverSocket = makeServerSocket();

            while (true) {
                // This will wait for a connection to be made to this socket.
                socket = serverSocket.accept();
                // serverSocket.accept();
                socket.setKeepAlive(true);

                // Recieve the client's request object
                InputStream inputStream = socket.getInputStream();
                ObjectInput objectInput = new ObjectInputStream(inputStream);
                ClientRequest clientRequest = (ClientRequest) objectInput.readObject();

                if (!clientRequest.checkPassphrase(Config.getStringValue("passphrase", false))) {
                    Main.infoMessage("Server: Client refused, wrong passphrase!");

                    // Check if a event has been modified!
                } else if (clientRequest.requestsNewEvents()) {
                    Main.infoMessage("Server: Client requests new events");

                    // Get all calendar events which match the request
                    Vector vecEvents = calendarDatabase.getMatchingEvents(clientRequest);
                    ServerResponse serverResponse = new ServerResponse(vecEvents);

                    OutputStream outputStream = socket.getOutputStream();
                    ObjectOutput objectOutput = new ObjectOutputStream(outputStream);
                    objectOutput.writeObject(serverResponse);
                    objectOutput.flush();
                    objectOutput.close();

                } else if (clientRequest.actionIs(ClientRequest.MODIFY_EVENT)) {
                    Main.infoMessage("Server: Client wants to modify an event");
                    calendarDatabase.modifyEvent(clientRequest);
                    // if (Config.getBooleanValue("client_run"))
                    calendarDatabase.flush();

                } else if (clientRequest.actionIs(ClientRequest.DELETE_EVENT)) {
                    Main.infoMessage("Server: Client wants to delete an event");
                    calendarDatabase.deleteEvent(clientRequest);
                    // if (Config.getBooleanValue("client_run"))
                    calendarDatabase.flush();

                } else if (clientRequest.actionIs(ClientRequest.ADD_EVENT)) {
                    Main.infoMessage("Server: Client wants to add an event");
                    calendarDatabase.addEvent(clientRequest);
                    // if (Config.getBooleanValue("client_run"))
                    calendarDatabase.flush();

                } else if (clientRequest.actionIs(ClientRequest.RENAME_CATEGORY)) {
                    Main.infoMessage("Server: Client wants to rename a category");
                    calendarDatabase.renameCategory(clientRequest);
                    // if (Config.getBooleanValue("client_run"))
                    calendarDatabase.flush();

                } else if (clientRequest.actionIs(ClientRequest.DELETE_CATEGORY)) {
                    Main.infoMessage("Server: Client wants to delete a category (NYI)");
                    calendarDatabase.deleteCategory(clientRequest);
                    // if (Config.getBooleanValue("client_run"))
                    calendarDatabase.flush();

                } else if (clientRequest.actionIs(ClientRequest.RELOAD_DATABASE)) {
                    // Read and parse the whole calendar database from file!
                    parser = new CalendarFormatParser();
                    parser.setWorkdir(sWorkdir);
                    parser.start();

                    calendarDatabase = new CalendarDatabase(parser.getCategories());

                } else if (clientRequest.actionIs(ClientRequest.FLUSH_DATABASE)) {
                    Main.infoMessage("Server: Client wants to flush the database");
                    calendarDatabase.flush();

                } else if (clientRequest.actionIs(ClientRequest.SHUTDOWN_SERVER)) {
                    Main.infoMessage("Server: Client wants the server to shut down");
                    calendarDatabase.flush();
                    socket.close();

                    if (Config.getBooleanValue("client_run")) {
                        Main.infoMessage("Server: Shutting down the server thread");
                        socket.close();
                        serverSocket.close();
                        break;
                    } else {
                        Main.exit(0);
                    }
                }

                socket.close();
            }

        } catch (ClassNotFoundException e) {
            Main.infoMessage("Error: Server error during serialization: " + e.getMessage());

        } catch (IOException e) {
            Main.infoMessage("Error: Server error during serialization: " + e.getMessage());
        }
    }
}
