
/* NetCalendar 2006, 2009 (c) Dipl.-Inform. (FH) Paul C. Buetow
 * http://buetow.org - netcalendar@dev.buetow.org
 */

/**
 */

package client;

import shared.*;
import shared.remotecall.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.util.Calendar;
import java.util.Locale;
import org.freixas.jcalendar.*;

/**
 * This is the class for the date picker
 * @author Paul C. Buetow
 */
public class JCalendarDatePicker extends SubWindow {
    private JCalendar jcalendar;
    private Calendar calendar;
    private CalendarEvent calendarEvent;

    /**
     * Creates a date picker. It can be used to pick a date.
     * @param netCalendarClient The NetCalendarClient object
     */
    public JCalendarDatePicker(NetCalendarClient netCalendarClient) {
        super("Calendar", netCalendarClient);

        initComponents();
        setResizable(false);
        pack();
        setVisible(true);
    }

    /**
     * Creates a date picker. It can be used to pick a date.
     * @param netCalendarClient The NetCalendarClient object
     */
    public JCalendarDatePicker(NetCalendarClient netCalendarClient,
                               CalendarEvent calendarEvent) {
        super("Set date for event #" + calendarEvent.getEventID() +
              ": " + calendarEvent.getDescription(), netCalendarClient);
        this.calendarEvent = calendarEvent;

        initComponents();
        setResizable(false);
        pack();
        setVisible(true);
    }

    /**
     * Inits the component
     */
    protected void initComponents() {
        JPanel contentPane = (JPanel) getContentPane();
        contentPane.setBackground(Color.BLACK);
        JCalendarDateListener listener = new JCalendarDateListener();

        jcalendar = new JCalendar(JCalendar.DISPLAY_DATE, false);
        if (calendarEvent != null)
            jcalendar.setDate(calendarEvent.getDate());
        jcalendar.addDateListener(listener);
        contentPane.add(jcalendar);
    }

    /**
     * Gets the current selected date of the date picker
     * @return Current selected date
     */
    public Calendar getSelectedDate() {
        synchronized (jcalendar) {
            return calendar;
        }
    }

    /**
     * Helper class for the DateListener of the JCalendar object
     */
    private class JCalendarDateListener implements DateListener {
        public void dateChanged(DateEvent dateEvent) {
            synchronized (jcalendar) {
                calendar = dateEvent.getSelectedDate();
                if (calendarEvent != null) {
                    calendarEvent.setDate(calendar.getTime());
                    ClientRequest clientRequest = new ClientRequest(ClientRequest.MODIFY_EVENT);
                    clientRequest.setEvent(calendarEvent);

                    ServerRequester.sendClientRequest(clientRequest);
                    netCalendarClient.updateLast();
                }
            }
        }

    }
}
