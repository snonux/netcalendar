/* NetCalendar 2006, 2009 (c) Dipl.-Inform. (FH) Paul C. Buetow
 * http://buetow.org - netcalendar@dev.buetow.org
 */

package client;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.*;


/**
 * This is the base class for all subwindows of a main session window of
 * the calendar client.
 * @author Paul C. Buetow
 *
 */
public abstract class SubWindow extends JFrame {
    protected final static long serialVersionUID = 1L;
    protected NetCalendarClient netCalendarClient;
    private SubWindow subWindow;

    /**
     * Creates the window and shows it.
     * @param sTitleText Specifies the title text of this JFrame.
     * @param netCalendarClient Specifies the calendar client session object to use.
     */
    public SubWindow(String sTitleText, NetCalendarClient netCalendarClient) {
        super(netCalendarClient.getSessionString() + sTitleText);
        netCalendarClient.addFrame(this);
        this.netCalendarClient = netCalendarClient;
        this.subWindow = this;
        this.addWindowListener(new MyWindowListener());
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(netCalendarClient);
    }

    /**
     * Creates the window and shows it.
     * @param sTitleText Specifies the title text of this JFrame.
     * @param netCalendarClient Specifies the calendar client session object to use.
     */
    public void init(String sTitleText, NetCalendarClient netCalendarClient) {
        initComponents();
        pack();
        setVisible(true);
    }

    /**
     * The implemented method should initialize all the GUI components.
     */
    protected abstract void initComponents();

    /**
     * This private class is only needed for removing the current JFrame of the outer class
     * from the NetCalendarClient object's frame Vector.
     * @author Paul C. Buetow
     */
    private class MyWindowListener implements WindowListener {
        /*
         *  (non-Javadoc)
         * @see java.awt.event.WindowListener#windowClosed(java.awt.event.WindowEvent)
         */
        public void windowClosed(WindowEvent e) {
            netCalendarClient.removeFrame(subWindow);
        }

        /*
         *  (non-Javadoc)
         * @see java.awt.event.WindowListener#windowActivated(java.awt.event.WindowEvent)
         */
        public void windowActivated(WindowEvent e) {	}

        /*
         *  (non-Javadoc)
         * @see java.awt.event.WindowListener#windowClosing(java.awt.event.WindowEvent)
         */
        public void windowClosing(WindowEvent e) {}

        /*
         *  (non-Javadoc)
         * @see java.awt.event.WindowListener#windowDeactivated(java.awt.event.WindowEvent)
         */
        public void windowDeactivated(WindowEvent e) {}

        /*
         *  (non-Javadoc)
         * @see java.awt.event.WindowListener#windowDeiconified(java.awt.event.WindowEvent)
         */
        public void windowDeiconified(WindowEvent e) {}

        /*
         *  (non-Javadoc)
         * @see java.awt.event.WindowListener#windowIconified(java.awt.event.WindowEvent)
         */
        public void windowIconified(WindowEvent e) {	}

        /*
         *  (non-Javadoc)
         * @see java.awt.event.WindowListener#windowOpened(java.awt.event.WindowEvent)
         */
        public void windowOpened(WindowEvent e) {}
    }
}
