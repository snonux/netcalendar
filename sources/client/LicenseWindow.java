/* NetCalendar 2006, 2009 (c) Dipl.-Inform. (FH) Paul C. Buetow
 * http://buetow.org - netcalendar@dev.buetow.org
 */

package client;

import shared.*;

/**
 * This window simply shows an license message.
 * @author Paul C. Buetow
 *
 */
public class LicenseWindow extends InfoWindow {
    final static long serialVersionUID = 1L;
    /**
       * Creates the window and shows it.
       * @param netCalendarClient Specifies the calendar client session object to use.
       */
    public LicenseWindow(NetCalendarClient netCalendarClient) {
        super(netCalendarClient, "License",
              Config.VERSION + "\n" +
              "Copyright (C) 2006, 2009 by Paul C. Buetow\n\n" +
              "This program is free software; you can redistribute it and/or " +
              "modify it under the terms of the GNU General Public License " +
              "as published by the Free Software Foundation; either version 2 " +
              "of the License, or (at your option) any later version.\n\n" +
              "This program is distributed in the hope that it will be useful, " +
              "but WITHOUT ANY WARRANTY; without even the implied warranty of " +
              "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the " +
              "GNU General Public License for more details.\n\n" +
              "You should have received a copy of the GNU General Public License " +
              "along with this program; if not, write to the Free Software " +
              "Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA."
             );
    }
}
