/* NetCalendar 2006, 2009 (c) Dipl.-Inform. (FH) Paul C. Buetow
 * http://buetow.org - netcalendar@dev.buetow.org
 */

package client.helper;

import javax.swing.*;
import javax.swing.SpringLayout;
import java.awt.*;

import shared.*;

/**
 * This helper class helps to create well arranged GUI components.
 * Its creating form- or grid-style layouts using the SpringLayout class.
 * @author Paul C. Buetow
 */
public class GUIHelper {
    /**
     * Aligns the first <code>iRows</code> * <code>iCols</code>
     * components of the parent in a grid. Each component is as big as the maximum
     * preferred width and height of the components.
     * The parent is made just big enough to fit them all.
     * @param containerParent Specifies the container to be used
     * @param iRows Specifies the number of rows to use.
     * @param iCols Specifies the number of columns to use.
     * @param iInitialX Specifies the x location to start the grid at.
     * @param iInitialY Specifies the y location to start the grid at.
     * @param iXPad Specifies the x padding between the cells.
     * @param iYPad Specifies the y padding between the cells
     */
    public static void makeGrid(Container containerParent,
                                int iRows, int iCols,
                                int iInitialX, int iInitialY,
                                int iXPad, int iYPad) {
        SpringLayout helper;
        try {
            helper = (SpringLayout)containerParent.getLayout();
        } catch (ClassCastException exc) {
            Main.infoMessage("Error: The first argument to makeGrid must use SpringLayout.");
            return;
        }

        Spring iXPadSpring = Spring.constant(iXPad);
        Spring iYPadSpring = Spring.constant(iYPad);
        Spring iInitialXSpring = Spring.constant(iInitialX);
        Spring iInitialYSpring = Spring.constant(iInitialY);
        int max = iRows * iCols;

        // Calculate Springs that are the max of the width/height so that all
        // cells have the same size.
        Spring maxWidthSpring = helper.getConstraints(containerParent.getComponent(0)).
                                getWidth();
        Spring maxHeightSpring = helper.getConstraints(containerParent.getComponent(0)).
                                 getWidth();
        for (int i = 1; i < max; i++) {
            SpringLayout.Constraints cons = helper.getConstraints(
                                                containerParent.getComponent(i));

            maxWidthSpring = Spring.max(maxWidthSpring, cons.getWidth());
            maxHeightSpring = Spring.max(maxHeightSpring, cons.getHeight());
        }

        // Apply the new width/height Spring. This forces all the
        // components to have the same size.
        for (int i = 0; i < max; i++) {
            SpringLayout.Constraints cons = helper.getConstraints(
                                                containerParent.getComponent(i));

            cons.setWidth(maxWidthSpring);
            cons.setHeight(maxHeightSpring);
        }

        // Then adjust the x/y constraints of all the cells so that they
        // are aligned in a grid.
        SpringLayout.Constraints lastCons = null;
        SpringLayout.Constraints lastRowCons = null;
        for (int i = 0; i < max; i++) {
            SpringLayout.Constraints cons = helper.getConstraints(
                                                containerParent.getComponent(i));
            if (i % iCols == 0) { //start of new iRow
                lastRowCons = lastCons;
                cons.setX(iInitialXSpring);
            } else { //x position depends on previous component
                cons.setX(Spring.sum(lastCons.getConstraint(SpringLayout.EAST),
                                     iXPadSpring));
            }

            if (i / iCols == 0) { //first iRow
                cons.setY(iInitialYSpring);
            } else { //y position depends on previous iRow
                cons.setY(Spring.sum(lastRowCons.getConstraint(SpringLayout.SOUTH),
                                     iYPadSpring));
            }
            lastCons = cons;
        }

        // Set the containerParent's size.
        SpringLayout.Constraints pCons = helper.getConstraints(containerParent);
        pCons.setConstraint(SpringLayout.SOUTH,
                            Spring.sum(Spring.constant(iYPad), lastCons.getConstraint(SpringLayout.SOUTH)));
        pCons.setConstraint(SpringLayout.EAST,
                            Spring.sum(Spring.constant(iXPad), lastCons.getConstraint(SpringLayout.EAST)));
    }

    /**
     * This is a helper method for makeGrid. It returns the constraints for a specific cell.
     * @param iRow Specifies the row to get the constraints for.
     * @param iCol Specifies the column to get the constraints for.
     * @param containerParent Specifies the parent container.
     * @param iCols Specifies the number of columns being used.
     * @return Returns the constraints object of the current SpringLayout.
     */
    private static SpringLayout.Constraints getConstraintsForCell(int iRow, int iCol,
            Container containerParent, int iCols) {

        SpringLayout helper = (SpringLayout) containerParent.getLayout();
        Component component = containerParent.getComponent(iRow * iCols + iCol);
        return helper.getConstraints(component);
    }

    /**
     * Aligns the first <code>iRows</code> * <code>iCols</code>
     * components of <code>containerParent</code> in
     * a grid. Each component in a iColumn is as wide as the maximum
     * preferred width of the components in that iColumn;
     * height is similarly determined for each iRow.
     * The containerParent is made just big enough to fit them all.
     * @param iRows Specifies number of rows.
     * @param iCols Specifies the number of columns.
     * @param iInitialX Specifies the x location to start the grid at.
     * @param iInitialY Specifies the y location to start the grid at.
     * @param iXPad Specifies the x padding between the cells.
     * @param iYPad Specifies the y padding between the cells.
     */
    public static void makeCompactGrid(Container containerParent,
                                       int iRows, int iCols,
                                       int iInitialX, int iInitialY,
                                       int iXPad, int iYPad) {
        SpringLayout helper;

        try {
            helper = (SpringLayout)containerParent.getLayout();
        } catch (ClassCastException exc) {
            Main.infoMessage("Error: The first argument to makeCompactGrid must use SpringLayout.");
            return;
        }

        // Align all cells in each iColumn and make them the same width.
        Spring x = Spring.constant(iInitialX);
        for (int c = 0; c < iCols; c++) {
            Spring width = Spring.constant(0);
            for (int r = 0; r < iRows; r++) {
                width = Spring.max(width,
                                   getConstraintsForCell(r, c, containerParent, iCols).getWidth());
            }

            for (int r = 0; r < iRows; r++) {
                SpringLayout.Constraints constraints =
                    getConstraintsForCell(r, c, containerParent, iCols);
                constraints.setX(x);
                constraints.setWidth(width);
            }
            x = Spring.sum(x, Spring.sum(width, Spring.constant(iXPad)));
        }

        // Align all cells in each iRow and make them the same height.
        Spring y = Spring.constant(iInitialY);
        for (int r = 0; r < iRows; r++) {
            Spring height = Spring.constant(0);

            for (int c = 0; c < iCols; c++) {
                height = Spring.max(height,
                                    getConstraintsForCell(r, c, containerParent, iCols).getHeight());
            }

            for (int c = 0; c < iCols; c++) {
                SpringLayout.Constraints constraints =
                    getConstraintsForCell(r, c, containerParent, iCols);

                constraints.setY(y);
                constraints.setHeight(height);
            }

            y = Spring.sum(y, Spring.sum(height, Spring.constant(iYPad)));
        }

        // Set the containerParent's size.
        SpringLayout.Constraints pCons = helper.getConstraints(containerParent);
        pCons.setConstraint(SpringLayout.SOUTH, y);
        pCons.setConstraint(SpringLayout.EAST, x);
    }
}
