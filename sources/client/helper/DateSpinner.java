/* NetCalendar 2006, 2009 (c) Dipl.-Inform. (FH) Paul C. Buetow
 * http://buetow.org - netcalendar@dev.buetow.org
 */

package client.helper;


import java.awt.FlowLayout;
import java.util.Calendar;
import java.util.Date;

import javax.swing.*;

/**
 * This helper class helps to create a date spinner to edit Date objects.
 * @author Paul C. Buetow
 */
public class DateSpinner extends JComponent {
    private final static long serialVersionUID = 1L;
    private Date date;
    private SpinnerDateModel spinnerDateModel;

    /**
     * Creates a date spinner to set/edit a given date. This constructor uses the
     * current date!
     */
    public DateSpinner() {
        this.date = new Date();
        initComponents();
    }

    /**
     * Creates a date spinner to set/edit a given date.
     * @param date The date to be used for the date spinner!
     */
    public DateSpinner(Date date) {
        this.date = date;
        initComponents();
    }

    /**
     * Initializes all the date spinner GUI components.
     */
    private void initComponents() {
        setLayout(new FlowLayout(FlowLayout.LEFT, 4, 4));

        spinnerDateModel = new SpinnerDateModel(date, null, null, Calendar.MONTH);
        JSpinner jSpinner = new JSpinner(spinnerDateModel);
        new JSpinner.DateEditor(jSpinner, "MM/yy");

        add(jSpinner);
    }

    /**
     * Returns the date which is represented by this spinner.
     * @return Returns the date which is represented by this spinner.
     */
    public Date getDate() {
        return spinnerDateModel.getDate();
    }
}
