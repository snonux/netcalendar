/* NetCalendar 2006, 2009 (c) Dipl.-Inform. (FH) Paul C. Buetow
 * http://buetow.org - netcalendar@dev.buetow.org
 */

package client;

import java.awt.*;
import javax.swing.*;
import shared.*;

public class SplashScreen extends JWindow implements Runnable {
    private static final long serialVersionUID = 1L;

    /**
     * A simple little method to show a title screen in the center
    	* of the screen for the amount of time given in the constructor
     */
    public void run() {
        JPanel jPanel = (JPanel)getContentPane();
        jPanel.setBackground(Color.BLACK);
        jPanel.setForeground(Color.WHITE);

        // Set the window's bounds, centering the window
        int iWidth = 411;
        int iHeight = 261;
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();

        int x = (dimension.width-iWidth)/2;
        int y = (dimension.height-iHeight)/2;
        setBounds(x,y,iWidth,iHeight);

        JLabel jLabel = new JLabel(new ImageIcon("images/netcal.png"));
        jPanel.add(jLabel, BorderLayout.CENTER);
        jPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
        setVisible(true);

        try {
            Thread.sleep(3000);

        } catch (Exception e) {
            Main.infoMessage(e.getMessage());
        }

        dispose();
    }
}
