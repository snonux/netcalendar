/* NetCalendar 2006, 2009 (c) Dipl.-Inform. (FH) Paul C. Buetow
 * http://buetow.org - netcalendar@dev.buetow.org
 */

/**
 */

package client;

import java.awt.*;
import java.awt.event.*;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.swing.*;
import javax.swing.table.*;

import shared.*;
import shared.remotecall.*;
import client.inputforms.*;

/**
 * This is the main class of the client part of the netcalendar suite. It contains the main GUI.
 * All subguis are created within this class.
 * @author Paul C. Buetow
 */
public class NetCalendarClient extends JFrame {
    private static final long serialVersionUID = 1L;
    private static int iNextSession = 1;
    private static int iNumCurrentSessions = 0;
    private int iCurrentSession;
    private NetCalendarClient netCalendarClient;
    private Vector vecFrames = new Vector();

    private Thread currentTimeThread;

    // Diverse components
    private ClientRequest lastClientRequest = null;
    protected int iCurrentMouseSelectedRow = 0;
    protected int iCurrentMouseSelectedCol = 0;

    // GUI components
    private JMenuBar jMenuBar;
    private JPopupMenu jPopupMenu;
    private CalendarTableModel tableModel;
    private JTable jTable;

    private JTextField jTextFieldRegexp;
    private JTextField jTextFieldStatusMessages;
    private JTextField jTextFieldCurrentTime;

    // Some callback objects
    private DoCallback doCallbackEditEvent;
    private DoCallback doCallbackEditEventDate;
    private DoCallback doCallbackDeleteEvent;
    private DoCallback doCallbackCopyEvent;
    private DoCallback doCallbackDeleteCategory;
    private DoCallback doCallbackRenameCategory;

    // Static GUI strings which needs to be specified at least twice
    private final static String DELETE_EVENT = "Delete event(s)";
    private final static String EDIT_EVENT = "Edit event(s)";
    private final static String EDIT_EVENT_DATE = "Edit event date(s)";
    private final static String COPY_EVENT = "Copy event(s)";
    private final static String CREATE_EVENT = "Create new event";
    private final static String DELETE_CATEGORY = "Delete whole category(s)";
    private final static String RENAME_CATEGORY = "Rename whole category(s)";
    private final static String ADVANCED_SEARCH = "Advanced searching";
    private final static String SORT_BY_COL = "Sort by this row";
    private final static String REVERSE_SORT_BY_COL = "Reverse sort by this row";
    private final static String ENTER_REGEXP_HERE ="Enter some regexp search string here...";
    private static final String DATE_FORMAT_NOW = "yyyy/MM/dd HH:mm:ss";

    /**
     * Standard constructor, creates the client GUI.
     */
    public NetCalendarClient() {
        super(getSessionString(iNextSession) + Config.VERSION);
        this.iCurrentSession = iNextSession++;

        // Save a reference of this to allow to access this object through
        // inner-classes
        // (See the "Advanced searching" action listener object for example!)
        this.netCalendarClient = this;
        iNumCurrentSessions++;

        initComponents();
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        pack();
        setVisible(true);

        // Start a default request to the netcalendar server
        update(new ClientRequest(ClientRequest.REQUEST_ALL_EVENTS));
    }

    /**
     * Initializes all the GUI components.
     */
    private void initComponents() {
        this.addWindowListener(new WindowListener() {
            public void windowActivated(WindowEvent e) {	}
            public void windowClosing(WindowEvent e) {}
            public void windowDeactivated(WindowEvent e) {}
            public void windowDeiconified(WindowEvent e) {}
            public void windowIconified(WindowEvent e) {	}
            public void windowOpened(WindowEvent e) {}
            public void windowClosed(WindowEvent e) {
                if (--iNumCurrentSessions == 0)
                    Main.exit(0);

                Main.infoMessage("Closing a session window");
            }
        });

        Container container = getContentPane();

        jMenuBar = new JMenuBar();
        setJMenuBar(jMenuBar);

        JMenu jMenuSession = new JMenu("Session");
        jMenuSession.setMnemonic(KeyEvent.VK_S);
        jMenuBar.add(jMenuSession);

        JMenuItem jMenuItemShowCalendar = new JMenuItem("Show calendar");
        jMenuItemShowCalendar.setMnemonic(KeyEvent.VK_S);
        jMenuSession.add(jMenuItemShowCalendar);
        jMenuItemShowCalendar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                new JCalendarDatePicker(netCalendarClient);
            }
        });

        jMenuSession.add(new JSeparator());

        JMenuItem jMenuItemNewWindow = new JMenuItem("New window");
        jMenuItemNewWindow.setMnemonic(KeyEvent.VK_N);
        jMenuSession.add(jMenuItemNewWindow);
        jMenuItemNewWindow.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                new NetCalendarClient();
            }
        });

        JMenuItem jMenuItemCloseWindow = new JMenuItem("Close window");
        jMenuItemCloseWindow.setMnemonic(KeyEvent.VK_C);
        jMenuSession.add(jMenuItemCloseWindow);
        jMenuItemCloseWindow.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                dispose();
            }
        });

        jMenuSession.add(new JSeparator());

        JMenuItem jMenuItemPreferences = new JMenuItem("Preferences");
        jMenuItemPreferences.setMnemonic(KeyEvent.VK_P);
        jMenuSession.add(jMenuItemPreferences);
        jMenuItemPreferences.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                new Preferences(netCalendarClient);
            }
        });

        jMenuSession.add(new JSeparator());

        JMenuItem jMenuItemQuit = new JMenuItem("Quit");
        jMenuItemQuit.setMnemonic(KeyEvent.VK_Q);
        jMenuSession.add(jMenuItemQuit);
        jMenuItemQuit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                System.exit(0);
            }
        });

        JMenu jMenuEdit = new JMenu("Edit");
        jMenuEdit.setMnemonic(KeyEvent.VK_E);
        jMenuBar.add(jMenuEdit);

        JMenuItem jMenuItemCreate = new JMenuItem(CREATE_EVENT);
        jMenuItemCreate.setMnemonic(KeyEvent.VK_C);
        jMenuEdit.add(jMenuItemCreate);
        jMenuItemCreate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                new CreateNewEvent(netCalendarClient);
            }
        });

        JMenuItem jMenuItemEdit = new JMenuItem(EDIT_EVENT);
        jMenuItemEdit.setMnemonic(KeyEvent.VK_E);
        jMenuEdit.add(jMenuItemEdit);
        doCallbackEditEvent = new DoCallback() {
            public void callback(Object obj) {
                new EditExistingEvent(netCalendarClient, (CalendarEvent) obj);
            }
        };
        doCallbackEditEventDate = new DoCallback() {
            public void callback(Object obj) {
                new JCalendarDatePicker(netCalendarClient, (CalendarEvent) obj);
            }
        };
        jMenuItemEdit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                foreachSelectedEvent(doCallbackEditEvent);
            }
        });

        JMenuItem jMenuItemCopy = new JMenuItem(COPY_EVENT);
        jMenuItemCopy.setMnemonic(KeyEvent.VK_O);
        jMenuEdit.add(jMenuItemCopy);
        doCallbackCopyEvent = new DoCallback() {
            public void callback(Object obj) {
                CalendarEvent calendarEvent = (CalendarEvent) obj;
                ClientRequest clientRequest = new ClientRequest(ClientRequest.ADD_EVENT);
                clientRequest.setEvent(calendarEvent);
                ServerRequester.sendClientRequest(clientRequest);
                netCalendarClient.updateLast();
            }
        };
        jMenuItemCopy.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                foreachSelectedEvent(doCallbackCopyEvent);
            }
        });

        JMenuItem jMenuItemDelete = new JMenuItem(DELETE_EVENT);
        jMenuItemDelete.setMnemonic(KeyEvent.VK_D);
        jMenuEdit.add(jMenuItemDelete);
        doCallbackDeleteEvent = new DoCallback() {
            public void callback(Object obj) {
                deleteEvent((CalendarEvent)obj);
                jTable.getSelectionModel().clearSelection();
            }
        };
        jMenuItemDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                foreachSelectedEvent(doCallbackDeleteEvent);
            }
        });

        jMenuEdit.add(new JSeparator());

        JMenuItem jMenuItemRenameCategory = new JMenuItem(RENAME_CATEGORY);
        jMenuItemRenameCategory.setMnemonic(KeyEvent.VK_R);
        jMenuEdit.add(jMenuItemRenameCategory);
        doCallbackRenameCategory = new DoCallback() {
            public void callback(Object obj) {
                new RenameCategory(netCalendarClient, (CalendarEvent) obj);
                jTable.getSelectionModel().clearSelection();
            }
        };
        jMenuItemRenameCategory.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                foreachSelectedEvent(doCallbackRenameCategory);
            }
        });

        JMenuItem jMenuItemDeleteCategory = new JMenuItem(DELETE_CATEGORY);
        jMenuItemDeleteCategory.setMnemonic(KeyEvent.VK_L);
        jMenuEdit.add(jMenuItemDeleteCategory);
        doCallbackDeleteCategory = new DoCallback() {
            public void callback(Object obj) {
                deleteCategory((CalendarEvent)obj);
                jTable.getSelectionModel().clearSelection();
            }
        };
        jMenuItemDeleteCategory.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                foreachSelectedEvent(doCallbackDeleteCategory);
            }
        });


        JMenu jMenuSorting = new JMenu("Sorting");
        jMenuSorting.setMnemonic(KeyEvent.VK_O);
        jMenuBar.add(jMenuSorting);

        JMenuItem jMenuItemSortByDate = new JMenuItem("Sort by date");
        jMenuItemSortByDate.setMnemonic(KeyEvent.VK_D);
        jMenuSorting.add(jMenuItemSortByDate);
        jMenuItemSortByDate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                tableModel.sortByDate();
            }
        });
        JMenuItem jMenuItemSortByCategory = new JMenuItem("Sort by category");
        jMenuItemSortByCategory.setMnemonic(KeyEvent.VK_C);
        jMenuSorting.add(jMenuItemSortByCategory);
        jMenuItemSortByCategory.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                tableModel.sortByCategory();
            }
        });
        JMenuItem jMenuItemSortByDescr = new JMenuItem("Sort by description");
        jMenuItemSortByDescr.setMnemonic(KeyEvent.VK_E);
        jMenuSorting.add(jMenuItemSortByDescr);
        jMenuItemSortByDescr.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                tableModel.sortByDescription();
            }
        });
        JMenuItem jMenuItemSortByPlace = new JMenuItem("Sort by place");
        jMenuItemSortByPlace.setMnemonic(KeyEvent.VK_P);
        jMenuSorting.add(jMenuItemSortByPlace);
        jMenuItemSortByPlace.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                tableModel.sortByPlace();
            }
        });
        JMenuItem jMenuItemSortByEventIDs = new JMenuItem("Sort by event IDs");
        jMenuItemSortByEventIDs.setMnemonic(KeyEvent.VK_I);
        jMenuSorting.add(jMenuItemSortByEventIDs);
        jMenuItemSortByEventIDs.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                tableModel.sortByID();
            }
        });

        jMenuSorting.add(new JSeparator());

        JMenuItem jMenuItemReverseSortByDate = new JMenuItem(
            "Reverse sort by date");
        jMenuItemReverseSortByDate.setMnemonic(KeyEvent.VK_A);
        jMenuSorting.add(jMenuItemReverseSortByDate);
        jMenuItemReverseSortByDate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                tableModel.reverseSortByDate();
            }
        });
        JMenuItem jMenuItemReverseSortByCategory = new JMenuItem(
            "Reverse sort by category");
        jMenuItemReverseSortByCategory.setMnemonic(KeyEvent.VK_T);
        jMenuSorting.add(jMenuItemReverseSortByCategory);
        jMenuItemReverseSortByCategory.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                tableModel.reverseSortByCategory();
            }
        });
        JMenuItem jMenuItemReverseSortByDescr = new JMenuItem(
            "Reverse sort by description");
        jMenuItemReverseSortByDescr.setMnemonic(KeyEvent.VK_S);
        jMenuSorting.add(jMenuItemReverseSortByDescr);
        jMenuItemReverseSortByDescr.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                tableModel.reverseSortByDescription();
            }
        });
        JMenuItem jMenuItemReverseSortByPlace = new JMenuItem(
            "Reverse sort by place");
        jMenuItemReverseSortByPlace.setMnemonic(KeyEvent.VK_L);
        jMenuSorting.add(jMenuItemReverseSortByPlace);
        jMenuItemReverseSortByPlace.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                tableModel.reverseSortByPlace();
            }
        });
        JMenuItem jMenuItemReverseSortByIDs = new JMenuItem(
            "Reverse sort by event IDs");
        jMenuItemReverseSortByIDs.setMnemonic(KeyEvent.VK_R);
        jMenuSorting.add(jMenuItemReverseSortByIDs);
        jMenuItemReverseSortByIDs.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                tableModel.reverseSortByID();
            }
        });

        JMenu jMenuServer = new JMenu("Server");
        jMenuServer.setMnemonic(KeyEvent.VK_R);
        jMenuBar.add(jMenuServer);

        JMenuItem jMenuItemUpdate = new JMenuItem("Update events from server");
        jMenuItemUpdate.setMnemonic(KeyEvent.VK_U);
        jMenuServer.add(jMenuItemUpdate);
        jMenuItemUpdate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                update(lastClientRequest);
            }
        });

        JMenuItem jMenuItemGetAll = new JMenuItem("Get all events from server");
        jMenuItemGetAll.setMnemonic(KeyEvent.VK_G);
        jMenuServer.add(jMenuItemGetAll);
        ActionListener actionListenerGetAll = new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                update(new ClientRequest(ClientRequest.REQUEST_ALL_EVENTS));
            }
        };
        jMenuItemGetAll.addActionListener(actionListenerGetAll);

        JMenuItem jMenuItemAdvancedSearch = new JMenuItem(
            "Advanced searching for events");
        jMenuItemAdvancedSearch.setMnemonic(KeyEvent.VK_A);
        jMenuServer.add(jMenuItemAdvancedSearch);
        jMenuItemAdvancedSearch.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                new AdvancedSearching(netCalendarClient);
            }
        });

        jMenuServer.add(new JSeparator());

        JMenuItem jMenuItemReloadDatabase = new JMenuItem("Reload database");
        jMenuItemReloadDatabase.setMnemonic(KeyEvent.VK_R);
        jMenuServer.add(jMenuItemReloadDatabase);
        jMenuItemReloadDatabase.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                ServerRequester.sendClientRequest(new ClientRequest(
                                                      ClientRequest.RELOAD_DATABASE));
                updateLast();
            }
        });
        JMenuItem jMenuItemFlushDatabase = new JMenuItem("Flush database");
        jMenuItemFlushDatabase.setMnemonic(KeyEvent.VK_F);
        jMenuServer.add(jMenuItemFlushDatabase);
        jMenuItemFlushDatabase.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                ServerRequester.sendClientRequest(new ClientRequest(
                                                      ClientRequest.FLUSH_DATABASE));
            }
        });

        jMenuServer.add(new JSeparator());

        JMenuItem jMenuItemShutdownServer = new JMenuItem("Shutdown server");
        jMenuItemShutdownServer.setMnemonic(KeyEvent.VK_S);
        jMenuServer.add(jMenuItemShutdownServer);
        jMenuItemShutdownServer.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                ServerRequester.sendClientRequest(new ClientRequest(
                                                      ClientRequest.SHUTDOWN_SERVER));
            }
        });

        JMenu jMenuAbout = new JMenu("About");
        jMenuAbout.setMnemonic(KeyEvent.VK_A);
        jMenuBar.add(jMenuAbout);

        JMenuItem jMenuItemAbout = new JMenuItem("About");
        jMenuItemAbout.setMnemonic(KeyEvent.VK_A);
        jMenuBar.add(jMenuAbout);
        jMenuAbout.add(jMenuItemAbout);
        jMenuItemAbout.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                new AboutWindow(netCalendarClient);
            }
        });
        JMenuItem jMenuItemLicense = new JMenuItem("License");
        jMenuItemLicense.setMnemonic(KeyEvent.VK_L);
        jMenuAbout.add(jMenuItemLicense);
        jMenuItemLicense.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                new LicenseWindow(netCalendarClient);
            }
        });

        jPopupMenu = new JPopupMenu();
        ActionListener jPopupMenuActionListener = new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                String sActionCommand = actionEvent.getActionCommand();
                if (sActionCommand.equals(DELETE_EVENT)) {
                    foreachSelectedEvent(doCallbackDeleteEvent);

                } else if (sActionCommand.equals(CREATE_EVENT)) {
                    new CreateNewEvent(netCalendarClient);

                } else if (sActionCommand.equals(EDIT_EVENT)) {
                    foreachSelectedEvent(doCallbackEditEvent);

                } else if (sActionCommand.equals(EDIT_EVENT_DATE)) {
                    foreachSelectedEvent(doCallbackEditEventDate);

                } else if (sActionCommand.equals(COPY_EVENT)) {
                    foreachSelectedEvent(doCallbackCopyEvent);

                } else if (sActionCommand.equals(RENAME_CATEGORY)) {
                    foreachSelectedEvent(doCallbackRenameCategory);

                } else if (sActionCommand.equals(DELETE_CATEGORY)) {
                    foreachSelectedEvent(doCallbackDeleteCategory);

                } else if (sActionCommand.equals(ADVANCED_SEARCH)) {
                    new AdvancedSearching(netCalendarClient);

                } else if (sActionCommand.equals(SORT_BY_COL)) {
                    tableModel.sortByCol(iCurrentMouseSelectedCol);

                } else if (sActionCommand.equals(REVERSE_SORT_BY_COL)) {
                    tableModel.reverseSortByCol(iCurrentMouseSelectedCol);
                }
            }
        };

        JMenuItem jMenuItemPopupCreate = new JMenuItem(CREATE_EVENT);
        jMenuItemPopupCreate.addActionListener(jPopupMenuActionListener);
        jPopupMenu.add(jMenuItemPopupCreate);
        JMenuItem jMenuItemPopupEdit = new JMenuItem(EDIT_EVENT);
        jMenuItemPopupEdit.addActionListener(jPopupMenuActionListener);
        jPopupMenu.add(jMenuItemPopupEdit);
        JMenuItem jMenuItemPopupEditDate = new JMenuItem(EDIT_EVENT_DATE);
        jMenuItemPopupEditDate.addActionListener(jPopupMenuActionListener);
        jPopupMenu.add(jMenuItemPopupEditDate);
        JMenuItem jMenuItemPopupCopy = new JMenuItem(COPY_EVENT);
        jMenuItemPopupCopy.addActionListener(jPopupMenuActionListener);
        jPopupMenu.add(jMenuItemPopupCopy);
        JMenuItem jMenuItemPopupDelete = new JMenuItem(DELETE_EVENT);
        jMenuItemPopupDelete.addActionListener(jPopupMenuActionListener);
        jPopupMenu.add(jMenuItemPopupDelete);
        jPopupMenu.add(new JSeparator());

        JMenuItem jMenuItemPopupRenameCategory = new JMenuItem(RENAME_CATEGORY);
        jMenuItemPopupRenameCategory.addActionListener(jPopupMenuActionListener);
        jPopupMenu.add(jMenuItemPopupRenameCategory);
        JMenuItem jMenuItemPopupDeleteCategory = new JMenuItem(DELETE_CATEGORY);
        jMenuItemPopupDeleteCategory.addActionListener(jPopupMenuActionListener);
        jPopupMenu.add(jMenuItemPopupDeleteCategory);
        jPopupMenu.add(new JSeparator());

        JMenuItem jMenuItemPopupSort = new JMenuItem(SORT_BY_COL);
        jMenuItemPopupSort.addActionListener(jPopupMenuActionListener);
        jPopupMenu.add(jMenuItemPopupSort);
        JMenuItem jMenuItemPopupReverseSort = new JMenuItem(REVERSE_SORT_BY_COL);
        jMenuItemPopupReverseSort.addActionListener(jPopupMenuActionListener);
        jPopupMenu.add(jMenuItemPopupReverseSort);
        jPopupMenu.add(new JSeparator());

        JMenuItem jMenuItemPopupAdvancedSearch = new JMenuItem(ADVANCED_SEARCH);
        jMenuItemPopupAdvancedSearch
        .addActionListener(jPopupMenuActionListener);
        jPopupMenu.add(jMenuItemPopupAdvancedSearch);

        jTable = new JTable();
        jTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        jTable.setColumnModel(new DefaultTableColumnModel() {
            private static final long serialVersionUID = 1L;
            // Dont allow column moving!
            public void moveColumn(int iColumnIndex, int iNewColumnIndex) {	}
        });

        tableModel = new CalendarTableModel(jTable);
        jTable.setModel(tableModel);
        jTable.addMouseListener(new MouseListener() {
            public void mouseClicked(MouseEvent mouseEvent) {
                iCurrentMouseSelectedRow = jTable.rowAtPoint(mouseEvent.getPoint());
                iCurrentMouseSelectedCol = jTable.columnAtPoint(mouseEvent.getPoint());

                if (mouseEvent.getButton() != MouseEvent.BUTTON1)
                    jPopupMenu.show(jTable, mouseEvent.getX(), mouseEvent.getY());
            }

            public void mouseEntered(MouseEvent event) {	}
            public void mouseExited(MouseEvent event) {}
            public void mousePressed(MouseEvent event) {	}
            public void mouseReleased(MouseEvent event) {
            }
        });

        CalendarTableCellRenderer cellRenderer = new CalendarTableCellRenderer(tableModel);

        TableColumn column = jTable.getColumn(CalendarTableModel.NUM_HEADER);
        column.setMaxWidth(Config.getStringValue("client_max_events").length() * 10);
        column.setResizable(false);
        column.setCellRenderer(cellRenderer);

        column = jTable.getColumn(CalendarTableModel.CATEGORY_HEADER);
        column.setPreferredWidth(10);
        column.setCellRenderer(cellRenderer);

        column = jTable.getColumn(CalendarTableModel.PLACE_HEADER);
        column.setPreferredWidth(10);
        column.setCellRenderer(cellRenderer);

        jTable.getColumn(CalendarTableModel.DATE_HEADER).setCellRenderer(cellRenderer);
        jTable.getColumn(CalendarTableModel.DESCRIPTION_HEADER).setCellRenderer(cellRenderer);

        jTextFieldRegexp = new JTextField();
        jTextFieldRegexp.setText(ENTER_REGEXP_HERE);
        jTextFieldRegexp.setEnabled(false);

        ActionListener actionListenerSearchRegexp = new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                ClientRequest clientRequest = new ClientRequest();
                clientRequest.setRegexpAll(jTextFieldRegexp.getText());
                clientRequest.setMainRegexp(true);
                jTextFieldRegexp.setText("");
                update(ServerRequester.sendClientRequest(clientRequest));
                lastClientRequest = clientRequest;
            }
        };
        MouseListener mouseListenerSearchRegexp = new MouseListener() {
            public void mouseClicked(MouseEvent mouseEvent) {}
            public void mousePressed(MouseEvent mouseEvent) {}
            public void mouseReleased(MouseEvent mouseEvent) {}
            public void mouseExited(MouseEvent mouseEvent) {
                if (jTextFieldRegexp.getText().equals("")) {
                    jTextFieldRegexp.setText(ENTER_REGEXP_HERE);
                    jTextFieldRegexp.setEnabled(false);
                }
            }
            public void mouseEntered(MouseEvent mouseEvent) {
                if (!jTextFieldRegexp.isEnabled()) {
                    jTextFieldRegexp.setText("");
                    jTextFieldRegexp.setEnabled(true);
                }
            }
        };

        jTextFieldRegexp.addActionListener(actionListenerSearchRegexp);
        jTextFieldRegexp.addMouseListener(mouseListenerSearchRegexp);

        JButton jButtonSearch = new JButton("Search");
        jButtonSearch.addActionListener(actionListenerSearchRegexp);
        jButtonSearch.addMouseListener(mouseListenerSearchRegexp);

        JButton jButtonGetAll = new JButton("Get all");
        jButtonGetAll.addActionListener(actionListenerGetAll);

        // Init split panes
        JSplitPane jSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        jSplitPane.setResizeWeight(1);
        jSplitPane.setDividerSize(0);

        JSplitPane jSplitPane2 = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        jSplitPane2.setResizeWeight(1);
        jSplitPane2.setDividerSize(0);

        JSplitPane jSplitPane3 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        jSplitPane3.setResizeWeight(1);
        jSplitPane3.setDividerSize(0);

        JSplitPane jSplitPane4 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        jSplitPane4.setDividerSize(0);

        // Init components
        JScrollPane jScrollPaneTable = new JScrollPane(jTable);

        jTextFieldStatusMessages = new JTextField("Welcome to "
                + Config.VERSION);
        jTextFieldStatusMessages.setEditable(false);

        jTextFieldCurrentTime = new JTextField();
        jTextFieldCurrentTime.setEditable(false);

        // Set split pane components
        JSplitPane jSplitPaneStatus =
            new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        jSplitPaneStatus.setDividerSize(0);
        jSplitPaneStatus.setResizeWeight(1);

        jSplitPaneStatus.setLeftComponent(jTextFieldStatusMessages);
        jSplitPaneStatus.setRightComponent(jTextFieldCurrentTime);

        jSplitPane.setTopComponent(jScrollPaneTable);
        jSplitPane.setBottomComponent(jSplitPane2);

        jSplitPane2.setTopComponent(jSplitPane3);
        jSplitPane2.setBottomComponent(jSplitPaneStatus);

        JPanel jPanelButtons = new JPanel();
        jPanelButtons.add(jButtonSearch);
        jPanelButtons.add(jButtonGetAll);

        jSplitPane3.setLeftComponent(jTextFieldRegexp);
        jSplitPane3.setRightComponent(jPanelButtons);

        container.add(jSplitPane);

        currentTimeThread = new Thread() {
            public void run() {
                while (true) {
                    updateCurrentTime();
                    try {
                        Thread.sleep(1000);
                    } catch (Exception e) {
                        System.err.println(e);
                    }
                }
            }
        };

        currentTimeThread.start();
    }

    /**
     * This method sends a client request object to the server and used the server response object
     * to update the JTable ith its new values using the table model.
     * @param clientRequest  Specifies the client request object to use for the updating.
     */
    public void update(ClientRequest clientRequest) {
        if (clientRequest == null)
            return;

        update(ServerRequester.sendClientRequest(clientRequest));
        lastClientRequest = clientRequest;
    }

    /**
     * This method sends the last client request object being used to the server again. If
     * If there is no last client request, nothing will be done.
     */
    public void updateLast() {
        if (lastClientRequest != null)
            update(ServerRequester.sendClientRequest(lastClientRequest));
    }

    /**
     * This method updates the GUI unsing a given server response object.
     * @param serverResponse Specifies the server response to use for the updating.
     */
    public void update(ServerResponse serverResponse) {
        if (serverResponse == null)
            return;

        tableModel.setEvents(serverResponse.getEvents());
    }

    /**
     * This method tells the calendar server to delete a given calendar event.
     * @param deleteEvent Specifies the calendar event to delete.
     */
    public void deleteEvent(CalendarEvent deleteEvent) {
        ClientRequest deleteRequest = new ClientRequest(
            ClientRequest.DELETE_EVENT);
        deleteRequest.setEvent(deleteEvent);
        ServerRequester.sendClientRequest(deleteRequest);
        updateLast();
    }

    /**
     * This method tells the calendar server to delete a given calendar category.
     * @param deleteEventsCategory Specifies the calendar event to delete its category!
     */
    public void deleteCategory(CalendarEvent deleteEventsCategory) {
        ClientRequest deleteRequest = new ClientRequest(
            ClientRequest.DELETE_CATEGORY);
        deleteRequest.setEvent(deleteEventsCategory);
        ServerRequester.sendClientRequest(deleteRequest);
        updateLast();
    }

    /**
     * If the client has several main windows open, then it will display a
     * session indicator so that the user knows which window belongs to which
     * session window!
     * @param iSession Specifies the session number of the current client window.
     * @return Returns the session indicator of the current client session.
     */
    private static String getSessionString(int iSession) {
        return iSession > 1 ? "[" + iSession + "] " : "";
    }

    /**
     * If the client has several main windows open, then it will display a
     * session indicator so that the user knows which window belongs to which
     * session window.
     * @return Returns the session indicator of the current client session.
     */
    public String getSessionString() {
        return iCurrentSession > 1 ? "[" + iCurrentSession + "] " : "";
    }

    /**
     * Runs a callback function on each selected event of the table.
     * @param doCallback Specifies the callback object to be used.
     */
    public void foreachSelectedEvent(DoCallback doCallback) {
        ListSelectionModel listSelectionModel = jTable.getSelectionModel();

        int iMinIndex = listSelectionModel.getMinSelectionIndex();
        int iMaxIndex = listSelectionModel.getMaxSelectionIndex();

        Vector vecEvents = new Vector();

        for (int i = iMinIndex; i <= iMaxIndex; ++i)
            if (listSelectionModel.isSelectedIndex(i))
                vecEvents.add(tableModel.getEvent(i));

        Enumeration enumEvents = vecEvents.elements();
        while (enumEvents.hasMoreElements())
            doCallback.callback(enumEvents.nextElement());
    }

    /**
     * This method is for various status messages. All messages will show up in the
     * status bar of the current client session window.
     * @param sMessage  Specifies the message to be displayed in the status bar.
     */
    public void statusMessage(String sMessage) {
        jTextFieldStatusMessages.setText(sMessage);
        jTextFieldStatusMessages.updateUI();
    }

    /**
     * This method updates the current time in the time frame in the GUI
     */
    public synchronized void updateCurrentTime() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
        jTextFieldCurrentTime.setText(sdf.format(cal.getTime()));
    }

    /**
     * This method disposes this JFrame window including all the JFrame windows which
     * belong to this session.
     */
    public void dispose() {
        Enumeration enumFrames = vecFrames.elements();

        while (enumFrames.hasMoreElements())
            ((JFrame) enumFrames.nextElement()).dispose();

        super.dispose();
    }

    /**
     * This method tells the main netcalendar client JFrame which sub JFrames are opened.
     * So that all the sub JFrames will be disposed as well if the main JFrame gets disposed.
     * A sub JFrame is for example a input form for advanced searching or the preferences dialog.
     * @param jFrame Specifies the frame object to add. .
     */
    public void addFrame(JFrame jFrame) {
        vecFrames.add(jFrame);
    }

    /**
     * This method tells the main netcalendar client JFrame which sub JFrames are opened.
     * So that all the sub JFrames will be disposed as well if the main JFrame gets disposed.
     * A sub JFrame is for example a input form for advanced searching or the preferences dialog.
     * @param jFrame Specifies the frame object to remove.
     */
    public void removeFrame(JFrame jFrame) {
        vecFrames.remove(jFrame);
    }
}
