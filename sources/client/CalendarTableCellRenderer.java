/* NetCalendar 2006, 2009 (c) Dipl.-Inform. (FH) Paul C. Buetow
 * http://buetow.org - netcalendar@dev.buetow.org
 */

package client;

import java.awt.*;

import javax.swing.*;
import javax.swing.table.*;
import java.util.*;

import shared.*;

/**
 * This class is responsible for the rendering of the JTable of the client gui which contains
 * all the events.
 * @author Paul C. Buetow
 */
public class CalendarTableCellRenderer extends DefaultTableCellRenderer {
    private static final long serialVersionUID = 1L;
    private CalendarTableModel tableModel;
    // private NetCalendarClient netCalendarClient;

    private static final Color SELECTED_BACKGROUND_COLOR;
    private static final Color SELECTED_FOREGROUND_COLOR;
    private static final Color ALREADY_OVER_BACKGROUND_COLOR;
    private static final Color ALREADY_OVER_FOREGROUND_COLOR;
    private static final Color STANDARD_FOREGROUND_COLOR;
    private static final Color BACKGROUND_COLOR_1_DAY;
    private static final Color BACKGROUND_COLOR_7_DAYS;
    private static final Color BACKGROUND_COLOR_28_DAYS;
    private static final Color BACKGROUND_COLOR_168_DAYS;
    private static final Color BACKGROUND_COLOR_365_DAYS;
    private static final Color BACKGROUND_COLOR_MT_365_DAYS;
    private static final Color YEARLY_BACKGROUND_COLOR;
    private static final Color NOT_YEARLY_BACKGROUND_COLOR;


    private static final long NANOSECONDS_1_DAY;
    private static final long NANOSECONDS_7_DAYS;
    private static final long NANOSECONDS_28_DAYS;
    private static final long NANOSECONDS_168_DAYS;
    private static final long NANOSECONDS_365_DAYS;

    // Initialize once, use often!
    static {
        SELECTED_BACKGROUND_COLOR = new Color(0, 0, 0);
        SELECTED_FOREGROUND_COLOR = new Color(255, 255, 255);
        STANDARD_FOREGROUND_COLOR = new Color(0, 0, 0);
        ALREADY_OVER_FOREGROUND_COLOR = SELECTED_BACKGROUND_COLOR;
        ALREADY_OVER_BACKGROUND_COLOR = SELECTED_FOREGROUND_COLOR;
        BACKGROUND_COLOR_1_DAY = new Color(0xff, 0x00, 0x00);
        BACKGROUND_COLOR_7_DAYS = new Color(0xff, 0x6c, 0x00);
        BACKGROUND_COLOR_28_DAYS = new Color(0xff, 0xa5, 0x00);
        BACKGROUND_COLOR_168_DAYS = new Color(0xe7, 0xa5, 0x5e);
        BACKGROUND_COLOR_365_DAYS = new Color(230, 218, 161);
        BACKGROUND_COLOR_MT_365_DAYS = new Color(0xa2, 0x9c, 0x90); // For more than 365 days

        YEARLY_BACKGROUND_COLOR = new Color(0x65, 0xa9, 0xe3);
        NOT_YEARLY_BACKGROUND_COLOR = new Color(0x88, 0xe0, 0x90);


        NANOSECONDS_1_DAY = 3600 * 24 * 1000;
        NANOSECONDS_7_DAYS = NANOSECONDS_1_DAY * 7;
        NANOSECONDS_28_DAYS = NANOSECONDS_7_DAYS * 4;
        NANOSECONDS_168_DAYS = NANOSECONDS_28_DAYS * 6;
        NANOSECONDS_365_DAYS = NANOSECONDS_1_DAY * 365;
    }

    /**
     * This method creates a custom table cell renderer for the calendar client.
     * Its coloring the different cells and sets other special display attributes.
     * @param tableModel Specifies the DefaultTableModel object used by the JTable. This is needed to make decisions about displaying a specific cell.
     */
    public CalendarTableCellRenderer(CalendarTableModel tableModel) {
        // this.netCalendarClient = netCalendarClient;
        this.tableModel = tableModel;
    }

    /**
     * This method returns the Component object of a specific table cell.
     * @param jTable Specifies the table object of the calendar client frame.
     * @param object Specifies the object which is inside of the specific table cell.
     * @param isSelected Specifies if the current cell is selected or not.
     * @param hasFocus Specifies if the current cell is focused or not.
     * @param iRow Specifies thr row number of the current cell.
     * @param iCol specifies the column number of the current cell.
     * @return Returns the Component object of a specific table cell.
     */
    public Component getTableCellRendererComponent(JTable jTable, Object object,
            boolean isSelected, boolean hasFocus, int iRow, int iCol) {
        JLabel jLable =(JLabel) super.getTableCellRendererComponent(jTable, object, isSelected, hasFocus, iRow, iCol);

        //MyVector vecSelected = netCalendarClient.getSelectedIndexes();

        //if (vecSelected.hasLike(new Integer(iRow))) {
        if (isSelected) {
            jLable.setBackground(SELECTED_BACKGROUND_COLOR);
            jLable.setForeground(SELECTED_FOREGROUND_COLOR);
            return jLable;
        }

        CalendarEvent calendarEvent = tableModel.getEvent(iRow);
        jLable.setForeground(STANDARD_FOREGROUND_COLOR);

        if (iCol <= 1) {
            if (!calendarEvent.isYearly())
                jLable.setBackground(NOT_YEARLY_BACKGROUND_COLOR);
            else
                jLable.setBackground(YEARLY_BACKGROUND_COLOR);

            return jLable;
        }

        long lCurrentTime = new Date().getTime();
        long lEventTime = calendarEvent.getDate().getTime();

        if (lCurrentTime + NANOSECONDS_1_DAY > lEventTime)
            jLable.setBackground(BACKGROUND_COLOR_1_DAY);

        else if (lCurrentTime + NANOSECONDS_7_DAYS > lEventTime)
            jLable.setBackground(BACKGROUND_COLOR_7_DAYS);

        else if (lCurrentTime + NANOSECONDS_28_DAYS > lEventTime)
            jLable.setBackground(BACKGROUND_COLOR_28_DAYS);

        else if (lCurrentTime + NANOSECONDS_168_DAYS > lEventTime)
            jLable.setBackground(BACKGROUND_COLOR_168_DAYS);

        else if (lCurrentTime + NANOSECONDS_365_DAYS > lEventTime)
            jLable.setBackground(BACKGROUND_COLOR_365_DAYS);

        else
            jLable.setBackground(BACKGROUND_COLOR_MT_365_DAYS);

        return jLable;
    }
}
