/* NetCalendar 2006, 2009 (c) Dipl.-Inform. (FH) Paul C. Buetow
 * http://buetow.org - netcalendar@dev.buetow.org
 */

package client;

import java.awt.*;
import javax.swing.*;

/**
 * This window simply shows an about message about the
 * netcalendar.
 * @author paul.buetow
 *
 */
public class InfoWindow extends SubWindow {
    final static long serialVersionUID = 1L;
    private String sText;

    /**
       * Creates the window and shows it.
       * @param netCalendarClient Specifies the calendar client session object to use.
       * @param jTitle Speicifies the title text to be displayd in the title bar.
       * @param jText Specifies the text to be displayed at the text area.
       */
    public InfoWindow(NetCalendarClient netCalendarClient, String jTitle, String sText) {
        super(jTitle, netCalendarClient);
        this.sText = sText;
        initComponents();
        setResizable(false);
        pack();
        setVisible(true);
    }

    /**
     * This method initializes all the GUI components.
     */
    protected void initComponents() {
        setSize(306,400);
        JPanel contentPane = (JPanel) getContentPane();
        contentPane.setBackground(Color.BLACK);

        // Build the splash screen
        JLabel jLabel = new JLabel(new ImageIcon("images/netcal.png"));
        contentPane.add(jLabel, BorderLayout.CENTER);

        JTextArea jTextArea = new JTextArea();
        jTextArea.setEditable(false);
        jTextArea.setBackground(Color.BLACK);
        jTextArea.setForeground(Color.WHITE);
        jTextArea.setLineWrap(true);
        jTextArea.setWrapStyleWord(true);
        jTextArea.setMargin(new Insets(10, 10, 10, 10));
        jTextArea.setText(sText);
        jTextArea.setFocusable(false);
        jTextArea.setEnabled(false);
        JScrollPane jScrollPane = new JScrollPane(jTextArea);
        jScrollPane.setPreferredSize(new Dimension(306, 400-261));
        contentPane.add(jScrollPane, BorderLayout.SOUTH);
    }
}
