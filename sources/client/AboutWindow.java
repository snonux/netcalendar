/* NetCalendar 2006, 2009 (c) Dipl.-Inform. (FH) Paul C. Buetow
 * http://buetow.org - netcalendar@dev.buetow.org
 */

package client;
import shared.*;

/**
 * This window simply shows an about message about the
 * netcalendar.
 * @author paul.buetow
 *
 */
public class AboutWindow extends InfoWindow {
    final static long serialVersionUID = 1L;
    /**
       * Creates the window and shows it.
       * @param sTitleText Specifies the title text of this JFrame.
       * @param netCalendarClient Specifies the calendar client session object to use.
       */
    public AboutWindow(NetCalendarClient netCalendarClient) {
        super(netCalendarClient, "About",
              Config.VERSION + "\n" +
              "(c) 2006, 2009 by Dipl.-Inform. (FH) Paul C. Buetow; " +
              "Web: buetow.org; E-Mail: " + getEmailAddr() + "\n\n" +
              "NetCalendar is a network capable and mostly UNIX /usr/bin/calendar database compatible Calendar application " +
              "programmed in Java. Its initial motivation was a programming project at the Aachen " +
              "University of Applied Sciences (www.fh-aachen.de) for the object oriented " +
              "programming class. But it became much more than just that!\n\n" +
              "Credits:\n\n" +
              "Bernhard Schertl " +
              "for testing and suggestions for improvements; Web: www.b78.org\n\n" +
              "Prof. Dr. rer. nat. H. Fassbender " +
              "for supervision of expert opinion; Web: www.fassbender.fh-aachen.de\n\n" +
              "Florian P. Buetow " +
              "for ideas concerning the splash image; Web: www.florianbuetow.com\n\n" +
              "Kay Czarnotta " +
              "for ideas concerning the icon and an optimization tip for NetCalendar for Mac OS X; Web: eagleeye.byte-welt.net"
             );
    }

    /**
     * This method simply returns a string containing the contact email address.
     * This method is needed to protect this sourcecode + the email address for
     * internet spam because some spambots try to fetch this address from the
     * cvsweb interface.
     * @return Returns a string containing the contact email address.
     */
    private static String getEmailAddr() {
        String sEmail = "netcalendar at dev dot buetow dot org";
        sEmail = sEmail.replaceAll(" at ", "@");
        return sEmail.replaceAll(" dot ", ".");
    }
}
