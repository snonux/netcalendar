/* NetCalendar 2006, 2009 (c) Dipl.-Inform. (FH) Paul C. Buetow
 * http://buetow.org - netcalendar@dev.buetow.org
 */

package client.inputforms;

import java.awt.event.*;
import java.util.*;

import javax.swing.*;

import client.NetCalendarClient;
import client.helper.GUIHelper;


import shared.*;
/**
 * This class contains all the GUI components of the preferences/options/config dialog.
 * Its used for editing the current config values of the netcalendar.conf file.
 * @author Paul C. Buetow
 *
 */
public class Preferences extends InputForm {
    private final static long serialVersionUID = 1L;

    private String[] labels = null;
    private int iNumPairs = -1;

    /**
     * Create the input form window and show it.
     * @param netCalendarClient Specifies the current calendar client session window.
     */
    public Preferences(NetCalendarClient netCalendarClient) {
        super("Preferences", netCalendarClient);
        initComponents();
        setFieldValues();
        pack();
        setVisible(true);
    }

    /**
     * Initializes all the GUI components.
     */
    protected void initComponents() {
        super.initComponents();
        setFieldValues();
        JPanel jPanel = new JPanel(new SpringLayout());

        labels = Config.getSortedKeyArray();
        iNumPairs = labels.length;

        ActionListener actionListenerTextFields = new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                submit();
            }
        };

        vecFields = new Vector();
        for (int i = 0; i < iNumPairs; ++i) {
            JLabel jLable = new JLabel(labels[i], JLabel.TRAILING);
            jPanel.add(jLable);
            JTextField textField = new JTextField(InputForm.TEXTFIELD_LENGTH);
            textField.addActionListener(actionListenerTextFields);
            jLable.setLabelFor(textField);
            jPanel.add(textField);
            vecFields.add(textField);
        }

        //Lay out the panel.
        GUIHelper.makeCompactGrid(jPanel,
                                  iNumPairs, 2, 	// iRows, iCols
                                  6, 6,        	// iInitX, iInitY
                                  6, 6);       	// iXPad, iYPad

        jPanelButtons.remove(jButtonClear);
        JSplitPane jSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        jSplitPane.setTopComponent(jPanel);
        jSplitPane.setBottomComponent(jPanelButtons);
        jSplitPane.setDividerSize(0);

        setContentPane(jSplitPane);
    }

    /**
     * This method sets the fields of the edit frame according to the current configuration options.
     */
    private void setFieldValues() {
        for (int i = 0; i < iNumPairs; ++i)
            ((JTextField) vecFields.get(i)).setText(Config.getStringValue(labels[i]));
    }

    /**
     * This method is invoked if the enter key is pressed or if the submit button
     * has been pressed. It starts a client request relating to the user's input of
     * the text fields. It will write all changes to the netcalendar.conf file.
     */
    protected void submit() {
        for (int i = 0; i < iNumPairs; ++i)
            Config.setValue(labels[i], ((JTextField) vecFields.get(i)).getText());

        Config.writeConfigToFile();
    }
}
