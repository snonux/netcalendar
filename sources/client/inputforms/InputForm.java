/* NetCalendar 2006, 2009 (c) Dipl.-Inform. (FH) Paul C. Buetow
 * http://buetow.org - netcalendar@dev.buetow.org
 */

package client.inputforms;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import javax.swing.*;

import client.*;

/**
 * This abstract class is the base class of all other classes of the same package.
 * It contains some common members using by all other (specialized) input form classes.
 * @author Paul C. Buetow
 *
 */
public abstract class InputForm extends SubWindow {
    protected final static long serialVersionUID = 1L;
    protected Vector vecFields;
    protected JPanel jPanelButtons;
    protected JButton jButtonClear;
    protected JButton jButtonApply;
    protected JButton jButtonCancel;
    protected JButton jButtonOK;
    private boolean bApplyHasBeenPressed = false;

    protected final static String BUTTON_CANCEL = "Cancel";
    protected final static String BUTTON_CLEAR = "Clear";
    protected final static String BUTTON_APPLY = "Apply";
    protected final static String BUTTON_OK = "OK";

    protected final static int TEXTFIELD_LENGTH = 20;

    /**
     * Creates the input form window and show it.
     * @param sTitleText Specifies the title text of this JFrame.
     * @param netCalendarClient Specifies the calendar client session object to use.
     */
    public InputForm(String sTitleText, NetCalendarClient netCalendarClient) {
        super(sTitleText, netCalendarClient);
    }

    /**
     * Initializes the input form
     * @param sTitleText Specifies the title text of this JFrame.
     * @param netCalendarClient Specifies the calendar client session object to use.
     */
    public void init(String sTitleText, NetCalendarClient netCalendarClient) {
        super.init(sTitleText, netCalendarClient);
    }

    /**
     * Initializes all the GUI components of the implementating class.
     */
    protected void initComponents() {
        jButtonClear = new JButton(BUTTON_CLEAR);
        jButtonApply = new JButton(BUTTON_APPLY);
        jButtonCancel = new JButton(BUTTON_CANCEL);
        jButtonOK = new JButton(BUTTON_OK);

        ActionListener actionListenerButtons = new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                if (event.getActionCommand().equals(BUTTON_CANCEL)) {
                    dispose();

                } else if (event.getActionCommand().equals(BUTTON_APPLY)) {
                    bApplyHasBeenPressed = true;
                    submit();

                } else if (event.getActionCommand().equals(BUTTON_OK)) {
                    if (!bApplyHasBeenPressed)
                        submit();
                    dispose();
                }
            }
        };

        jButtonCancel.addActionListener(actionListenerButtons);
        jButtonClear.addActionListener(actionListenerButtons);
        jButtonApply.addActionListener(actionListenerButtons);
        jButtonOK.addActionListener(actionListenerButtons);

        jPanelButtons = new JPanel();
        jPanelButtons.add(jButtonOK);
        jPanelButtons.add(jButtonCancel);
        jPanelButtons.add(jButtonApply);
        jPanelButtons.add(jButtonClear);
    }

    /**
     * Submits the input form of the implementating class.
     */
    protected abstract void submit();
}
