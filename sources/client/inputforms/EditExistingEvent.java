/* NetCalendar 2006, 2009 (c) Dipl.-Inform. (FH) Paul C. Buetow
 * http://buetow.org - netcalendar@dev.buetow.org
 */

package client.inputforms;

import java.awt.event.*;
import java.util.*;

import javax.swing.*;

import client.NetCalendarClient;
import client.ServerRequester;
import client.helper.DateSpinner;
import client.helper.GUIHelper;


import shared.*;
import shared.remotecall.*;

/**
 * This class contains all the GUI components of the edit event dialog.
 * Its used for editing existing events of the calendar database.
 * @author Paul C. Buetow
 *
 */
public class EditExistingEvent extends InputForm {
    private final static long serialVersionUID = 1L;

    // Static elements which are the same on all AdvancedSearching objects!
    private final static String BUTTON_DELETE = "Delete";
    private final static String[] labels =
        { "Event ID: ", "Description: ", "Category: ", "Place: ", "Yearly: ", "Date: "};
    private final static int iNumPairs = labels.length;
    private final static int iTextFields = iNumPairs - 2;
    private final static int iCheckBoxes = iNumPairs - 1;

    private CalendarEvent originalCalendarEvent;
    private Date date;

    /**
     * Create the input form window and show it.
     * @param netCalendarClient Specifies the current calendar client session window.
     * @param originalCalendarEvent Specifies the calendar event to modify.
     */
    public EditExistingEvent(NetCalendarClient netCalendarClient, CalendarEvent originalCalendarEvent) {
        super("Edit event", netCalendarClient);
        this.originalCalendarEvent = originalCalendarEvent;
        this.date = originalCalendarEvent.getDate();
        initComponents();
        setFieldValues();
        pack();
        setVisible(true);
    }

    /**
     * Initializes all the GUI components.
     */
    protected void initComponents() {
        super.initComponents();

        JPanel jPanel = new JPanel(new SpringLayout());

        ActionListener actionListenerTextFields = new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                submit();
            }
        };

        vecFields = new Vector();
        for (int i = 0; i < iNumPairs; ++i) {
            JLabel jLable = new JLabel(labels[i], JLabel.TRAILING);
            jPanel.add(jLable);
            JComponent jComponent = null;
            if ( i < iTextFields) {
                JTextField textField = new JTextField(InputForm.TEXTFIELD_LENGTH);
                textField.addActionListener(actionListenerTextFields);
                jComponent = textField;

            } else if (i < iCheckBoxes) {
                jComponent = new JCheckBox();

            } else {
                jComponent = new DateSpinner(date);
            }

            jLable.setLabelFor(jComponent);
            jPanel.add(jComponent);
            vecFields.add(jComponent);
        }

        //Lay out the panel.
        GUIHelper.makeCompactGrid(jPanel,
                                  iNumPairs, 2, 	// iRows, iCols
                                  6, 6,        	// iInitX, iInitY
                                  6, 6);       	// iXPad, iYPad

        JButton jButtonDelete = new JButton(BUTTON_DELETE);

        ActionListener actionListenerButtons = new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                if (event.getActionCommand().equals(BUTTON_CLEAR)) {
                    for (int i = 1; i < iNumPairs -2; ++i)
                        ((JTextField) vecFields.get(i)).setText("");

                } else if (event.getActionCommand().equals(BUTTON_DELETE)) {
                    netCalendarClient.deleteEvent(originalCalendarEvent);
                    dispose();
                }
            }
        };

        jButtonDelete.addActionListener(actionListenerButtons);
        jButtonClear.addActionListener(actionListenerButtons);
        jPanelButtons.add(jButtonDelete);

        JSplitPane jSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        jSplitPane.setTopComponent(jPanel);
        jSplitPane.setBottomComponent(jPanelButtons);
        jSplitPane.setDividerSize(0);

        setContentPane(jSplitPane);
    }

    /**
     * This method sets the fields of the edit frame according to the originalCalendarEvent object.
     * The date is not set by this method. Its done by the initComponents method.
     */
    private void setFieldValues() {
        JTextField jTextFieldEventID = (JTextField) vecFields.get(0);
        jTextFieldEventID.setText(""+originalCalendarEvent.getEventID());
        jTextFieldEventID.setEditable(false);

        ((JTextField) vecFields.get(1)).setText(originalCalendarEvent.getDescription());
        ((JTextField) vecFields.get(2)).setText(originalCalendarEvent.getCategoryName());
        ((JTextField) vecFields.get(3)).setText(originalCalendarEvent.getPlace());
        ((JCheckBox) vecFields.get(4)).setSelected(originalCalendarEvent.isYearly());
    }

    /**
     * This method is invoked if the enter key is pressed or if the submit button
     * has been pressed. It starts a client request relating to the user's input of
     * the text fields.
     */
    protected void submit() {
        String sCategoryName = ((JTextField) vecFields.get(2)).getText();
        CalendarEvent calendarEvent = new CalendarEvent(sCategoryName);
        calendarEvent.setDescription(((JTextField) vecFields.get(1)).getText());
        calendarEvent.setPlace(((JTextField) vecFields.get(3)).getText());
        calendarEvent.setYearly(((JCheckBox) vecFields.get(4)).isSelected());
        calendarEvent.setDate(((DateSpinner) vecFields.get(5)).getDate());
        calendarEvent.setEventID(originalCalendarEvent.getEventID());

        ClientRequest clientRequest = new ClientRequest(ClientRequest.MODIFY_EVENT);
        clientRequest.setEvent(calendarEvent);

        ServerRequester.sendClientRequest(clientRequest);
        netCalendarClient.updateLast();
    }
}
