/* NetCalendar 2006, 2009 (c) Dipl.-Inform. (FH) Paul C. Buetow
 * http://buetow.org - netcalendar@dev.buetow.org
 */

package client.inputforms;

import java.awt.event.*;
import java.util.*;

import javax.swing.*;

import client.NetCalendarClient;
import client.ServerRequester;
import client.helper.DateSpinner;
import client.helper.GUIHelper;


import shared.*;
import shared.remotecall.*;

/**
 * This class contains all the GUI components of the create new event dialog.
 * Its used for adding new calendar events to the database.
 * @author Paul C. Buetow
 *
 */
public class CreateNewEvent extends InputForm {
    private final static long serialVersionUID = 1L;

    // Static elements which are the same on all AdvancedSearching objects!
    private final static String[] labels =
        { "Description: ", "Category: ", "Place: ", "Yearly: ", "Date: "};
    private final static int iNumPairs = labels.length;
    private final static int iTextFields = iNumPairs - 2;
    private final static int iCheckBoxes = iNumPairs - 1;

    /**
     * Create the input form window and show it.
     * @param netCalendarClient Specifies the current calendar client session window.     *
     */
    public CreateNewEvent(NetCalendarClient netCalendarClient) {
        super("Create new event", netCalendarClient);
        initComponents();
        pack();
        setVisible(true);
    }

    /**
     * Initializes all the GUI components.
     */
    protected void initComponents() {
        super.initComponents();
        JPanel jPanel = new JPanel(new SpringLayout());

        ActionListener actionListenerTextFields = new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                submit();
            }
        };

        vecFields = new Vector();
        for (int i = 0; i < iNumPairs; ++i) {
            JLabel jLable = new JLabel(labels[i], JLabel.TRAILING);
            jPanel.add(jLable);
            JComponent jComponent = null;
            if ( i < iTextFields) {
                JTextField textField = new JTextField(InputForm.TEXTFIELD_LENGTH);
                textField.addActionListener(actionListenerTextFields);
                jComponent = textField;

            } else if (i < iCheckBoxes) {
                jComponent = new JCheckBox();

            } else {
                jComponent = new DateSpinner();
            }

            jLable.setLabelFor(jComponent);
            jPanel.add(jComponent);
            vecFields.add(jComponent);
        }

        //Lay out the panel.
        GUIHelper.makeCompactGrid(jPanel,
                                  iNumPairs, 2, 	// iRows, iCols
                                  6, 6,        	// iInitX, iInitY
                                  6, 6);       	// iXPad, iYPad

        ActionListener actionListenerButtons = new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                if (event.getActionCommand().equals(BUTTON_CLEAR))
                    for (int i = 0; i < iNumPairs -2; ++i)
                        ((JTextField) vecFields.get(i)).setText("");
            }
        };

        jButtonClear.addActionListener(actionListenerButtons);

        JSplitPane jSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        jSplitPane.setTopComponent(jPanel);
        jSplitPane.setBottomComponent(jPanelButtons);
        jSplitPane.setDividerSize(0);

        setContentPane(jSplitPane);
    }

    /**
     * This method is invoked if the enter key is pressed or if the submit button
     * has been pressed. It starts a client request relating to the user's input of
     * the text fields.
     */
    protected void submit() {
        String sCategoryName = ((JTextField) vecFields.get(1)).getText();
        CalendarEvent calendarEvent = new CalendarEvent(sCategoryName);
        calendarEvent.setDescription(((JTextField) vecFields.get(0)).getText());
        calendarEvent.setPlace(((JTextField) vecFields.get(2)).getText());
        calendarEvent.setYearly(((JCheckBox) vecFields.get(3)).isSelected());
        calendarEvent.setDate(((DateSpinner) vecFields.get(4)).getDate());

        ClientRequest clientRequest = new ClientRequest(ClientRequest.ADD_EVENT);
        clientRequest.setEvent(calendarEvent);

        ServerRequester.sendClientRequest(clientRequest);
        netCalendarClient.updateLast();
    }
}
