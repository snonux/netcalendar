/* NetCalendar 2006, 2009 (c) Dipl.-Inform. (FH) Paul C. Buetow
 * http://buetow.org - netcalendar@dev.buetow.org
 */

/*
 * A 1.4 application that uses SpringLayout to create a forms-type layout.
 * Other files required: SpringUtilities.java.
 */

package client.inputforms;

import java.awt.event.*;
import java.util.*;
import javax.swing.*;

import client.NetCalendarClient;
import client.helper.DateSpinner;
import client.helper.GUIHelper;


import shared.remotecall.*;

/**
 * This class contains all the GUI components of the advanced searching dialog.
 * Its used for using the andvanced searching options of the client such as using
 * Java regular expressions on specific elements of the calendar database instead and
 * date ranges.
 * @author Paul C. Buetow
 *
 */
public class AdvancedSearching extends InputForm {
    private final static long serialVersionUID = 1L;

    // Static elements which are the same on all AdvancedSearching objects!
    private final static String BUTTON_GET_ALL = "Get all";
    private final static String[] labels = {"Date string: ", "Description: ", "Category: ", "Place: ",
                                            "Use date ranging: ", "Date range from: ", "Date range to: "
                                           };
    private final static int iNumPairs = labels.length;
    private final static int iTextFields = labels.length - 3;
    private final static int iCheckBoxes = labels.length - 2;

    /**
     * Create the input form window and show it.
     * @param netCalendarClient Specifies the current calendar client session window.
     */
    public AdvancedSearching(NetCalendarClient netCalendarClient) {
        super("Advanced searching", netCalendarClient);
        initComponents();
        pack();
        setVisible(true);
    }

    /**
     * Initializes all the GUI components.
     */
    protected void initComponents() {
        super.initComponents();
        //Create and populate the panel.
        JPanel jPanel = new JPanel(new SpringLayout());

        ActionListener actionListenerFields = new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                submit();
            }
        };

        vecFields = new Vector();
        for (int i = 0; i < iNumPairs; ++i) {
            JLabel jLable = new JLabel(labels[i], JLabel.TRAILING);
            jPanel.add(jLable);
            JComponent jComponent = null;

            if (i < iTextFields) {
                JTextField jTextField = new JTextField(InputForm.TEXTFIELD_LENGTH);
                jTextField.addActionListener(actionListenerFields);
                jComponent = jTextField;

            } else if (i < iCheckBoxes) {
                jComponent = new JCheckBox();

            } else {
                jComponent = new DateSpinner();
            }

            jLable.setLabelFor(jComponent);
            jPanel.add(jComponent);

            vecFields.add(jComponent);
        }

        //Lay out the panel.
        GUIHelper.makeCompactGrid(jPanel,
                                  iNumPairs, 2, 	// iRows, iCols
                                  6, 6,        	// iInitX, iInitY
                                  6, 6);       	// iXPad, iYPad

        JButton jButtonGetAll = new JButton(BUTTON_GET_ALL);

        ActionListener actionListenerButtons = new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                if (event.getActionCommand().equals(BUTTON_CLEAR))
                    for (int i = 0; i < iTextFields; ++i)
                        ((JTextField) vecFields.get(i)).setText("");

                else if (event.getActionCommand().equals(BUTTON_GET_ALL))
                    netCalendarClient.update(new ClientRequest(ClientRequest.REQUEST_ALL_EVENTS));

            }
        };

        jButtonClear.addActionListener(actionListenerButtons);
        jButtonGetAll.addActionListener(actionListenerButtons);
        jPanelButtons.add(jButtonGetAll);

        JSplitPane jSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        jSplitPane.setTopComponent(jPanel);
        jSplitPane.setBottomComponent(jPanelButtons);
        jSplitPane.setDividerSize(0);

        setContentPane(jSplitPane);
    }

    /**
     * This method is invoked if the enter key is pressed or if the submit button
     * has been pressed. It starts a client request relating to the user's input of
     * the text fields.
     */
    protected void submit() {
        ClientRequest clientRequest = new ClientRequest();
        clientRequest.setRegexpDate(((JTextField) vecFields.get(0)).getText());
        clientRequest.setRegexpDescription(((JTextField) vecFields.get(1)).getText());
        clientRequest.setRegexpCategory(((JTextField) vecFields.get(2)).getText());
        clientRequest.setRegexpPlace(((JTextField) vecFields.get(3)).getText());

        JCheckBox jCheckBox = (JCheckBox) vecFields.get(4);
        if (jCheckBox.isSelected()) {
            Date dateRangeFrom = ((DateSpinner) vecFields.get(5)).getDate();
            Date dateRangeTo = ((DateSpinner) vecFields.get(6)).getDate();
            clientRequest.setDateRange(dateRangeFrom, dateRangeTo);
        }

        netCalendarClient.update(clientRequest);
    }
}
