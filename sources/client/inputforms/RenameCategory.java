/* NetCalendar 2006, 2009 (c) Dipl.-Inform. (FH) Paul C. Buetow
 * http://buetow.org - netcalendar@dev.buetow.org
 */

package client.inputforms;

import java.awt.event.*;
import java.util.*;

import javax.swing.*;

import client.NetCalendarClient;
import client.ServerRequester;
import client.helper.DateSpinner;
import client.helper.GUIHelper;


import shared.*;
import shared.remotecall.*;

/**
 * This class contains all the GUI components of the edit event dialog.
 * Its used for editing existing events of the calendar database.
 * @author Paul C. Buetow
 *
 */
public class RenameCategory extends InputForm {
    private final static long serialVersionUID = 1L;

    // Static elements which are the same on all AdvancedSearching objects!
    private final static String[] labels =	{ "Category name: " };
    private final static int iNumPairs = labels.length;

    private CalendarEvent calendarEvent;

    /**
     * Create the input form window and show it.
     * @param netCalendarClient Specifies the current calendar client session window.
     * @param originalCalendarEvent Specifies the calendar event to modify.
     */
    public RenameCategory(NetCalendarClient netCalendarClient, CalendarEvent calendarEvent) {
        super("Rename whole category", netCalendarClient);
        this.calendarEvent = calendarEvent;
        initComponents();
        setFieldValues();
        pack();
        setVisible(true);
    }

    /**
     * Initializes all the GUI components.
     */
    protected void initComponents() {
        super.initComponents();

        JPanel jPanel = new JPanel(new SpringLayout());

        ActionListener actionListenerTextFields = new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                submit();
            }
        };

        vecFields = new Vector();

        for (int i = 0; i < iNumPairs; ++i) {
            JLabel jLable = new JLabel(labels[i], JLabel.TRAILING);
            jPanel.add(jLable);
            JComponent jComponent = null;
            JTextField textField = new JTextField(InputForm.TEXTFIELD_LENGTH);
            textField.addActionListener(actionListenerTextFields);
            jComponent = textField;
            jLable.setLabelFor(jComponent);
            jPanel.add(jComponent);
            vecFields.add(jComponent);
        }

        //Lay out the panel.
        GUIHelper.makeCompactGrid(jPanel,
                                  iNumPairs, 2, 	// iRows, iCols
                                  6, 6,        	// iInitX, iInitY
                                  6, 6);       	// iXPad, iYPad

        ActionListener actionListenerButtons = new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                if (event.getActionCommand().equals(BUTTON_CLEAR)) {
                    for (int i = 1; i < iNumPairs; ++i)
                        ((JTextField) vecFields.get(i)).setText("");
                }
            }
        };

        jButtonClear.addActionListener(actionListenerButtons);
        jPanelButtons.remove(jButtonClear);

        JSplitPane jSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        jSplitPane.setTopComponent(jPanel);
        jSplitPane.setBottomComponent(jPanelButtons);
        jSplitPane.setDividerSize(0);

        setContentPane(jSplitPane);
    }

    /**
     * This method sets the fields of the edit frame according to the originalCalendarEvent object.
     * The date is not set by this method. Its done by the initComponents method.
     */
    private void setFieldValues() {
        ((JTextField) vecFields.get(0)).setText(calendarEvent.getCategoryName());
    }

    /**
     * This method is invoked if the enter key is pressed or if the submit button
     * has been pressed. It starts a client request relating to the user's input of
     * the text fields.
     */
    protected void submit() {
        String sNewCategoryName = ((JTextField) vecFields.get(0)).getText();

        ClientRequest clientRequest = new ClientRequest(ClientRequest.RENAME_CATEGORY);
        clientRequest.setEvent(calendarEvent);
        clientRequest.setString(sNewCategoryName);
        ServerRequester.sendClientRequest(clientRequest);
        netCalendarClient.updateLast();
        calendarEvent.setCategoryName(sNewCategoryName);
    }
}
