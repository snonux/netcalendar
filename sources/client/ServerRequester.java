/* NetCalendar 2006, 2009 (c) Dipl.-Inform. (FH) Paul C. Buetow
 * http://buetow.org - netcalendar@dev.buetow.org
 */

/**
 *
 */
package client;


import java.io.*;
import java.net.*;
import java.security.*;
import javax.net.*;
import javax.net.ssl.*;

import shared.*;
import shared.remotecall.*;

/**
 * This class only has static members. Its used for creating a client socket to connect and
 * communicate with the calendar server.
 * @author Paul C. Buetow
 */
public class ServerRequester  {
    /**
     * This method creates a new client socket.
     * @return Returns a client socket object.
     * @throws IOException
     */
    private static Socket makeClientSocket() throws IOException {
        if (!Config.getBooleanValue("use_ssl")) {
            Main.statusMessage("Making non-SSL Socket...");
            return new Socket(Config.getServerAddress(), Config.getIntValue("server_port"));
        }

        Main.statusMessage("Making SSL Socket...");
        //Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
        SocketFactory socketFactory = SSLSocketFactory.getDefault();
        return socketFactory.createSocket(Config.getServerAddress(), Config.getIntValue("server_port"));
    }

    /**
     * Sends a client request to the netcalendar server end receives a server response object.
     * @param clientRequest Specifies the client request to send to the calendar server.
     * @return Returns the server response. It returns null if no response is available.
     */
    public final static ServerResponse sendClientRequest(ClientRequest clientRequest) {
        Main.statusMessage("Sending client request to server...");

        ServerResponse serverResponse = null;

        try {
            Socket socket = makeClientSocket();
            socket.setKeepAlive(true);

            // Send the client request to the server
            OutputStream outputStream = socket.getOutputStream();
            ObjectOutput objectOutput = new ObjectOutputStream(outputStream);

            objectOutput.writeObject(clientRequest);
            objectOutput.flush();

            // We only want to receive data if the request new events, not if we only send
            // changed events!
            if (clientRequest.requestsNewEvents()) {
                // Recieve the server's response
                InputStream inputStream = socket.getInputStream();
                ObjectInput objectInput = new ObjectInputStream(inputStream);
                serverResponse = (ServerResponse) objectInput.readObject();
            }

            objectOutput.close();
            socket.close();

        } catch (Exception e) {
            Main.infoMessage("Client error during serialization: " + e.getMessage());
            Main.statusMessage("Error: Client request did not succeed! Server down?");
            return serverResponse;
        }

        Main.statusMessage("Last client request succeeded");
        // Returns null if no response is available!
        return serverResponse;
    }
}
