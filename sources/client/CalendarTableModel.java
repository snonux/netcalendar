/* NetCalendar 2006, 2009 (c) Dipl.-Inform. (FH) Paul C. Buetow
 * http://buetow.org - netcalendar@dev.buetow.org
 */

package client;

import javax.swing.table.*;
import javax.swing.*;
import java.util.*;
import shared.*;
import shared.remotecall.*;

/**
 * This class defined the table model of the JTable of the main calendar client window. All table
 * data and most of the table actions, like sorting or updating the content, go trough this class.
 * @author Paul C. Buetow
 *
 */
public final class CalendarTableModel extends AbstractTableModel {
    public static final String NUM_HEADER = "#";
    public static final String DATE_HEADER = "Date";
    public static final String CATEGORY_HEADER = "Category";
    public static final String DESCRIPTION_HEADER = "Description";
    public static final String PLACE_HEADER = "Place";

    private static final int iEnumLength = Config.getStringValue("client_max_events").length();
    private static final long serialVersionUID = 1L;
    private static final int iCols = 5; // Num of columns never changes!
    private int iRows = 0;
    private Vector vecEvents;
    private Comparator lastSortComparator = null;
    private JTable jTable;

    /**
     * Simple constructor, creates a calendar table model to be used with the JTable of the netcalendar client main window.
     * @param jTable Specifies the JTable object of this table model.
     */
    public CalendarTableModel(JTable jTable) {
        this.jTable = jTable;
    }

    /**
     * This method returns the number of columns of the JTable.
     * @return Returns the number of columns of the JTable.
     */
    public int getColumnCount() {
        return iCols;
    }

    /**
     * This method retuns the number of rows of the JTable.
     * @return Returns the number of rows of the JTable.
     */
    public int getRowCount() {
        return iRows;
    }

    /**
     * This method sets the number of rows of the JTable.
     * @param iRows Specifies the number of rows of the JTable.
     */
    public void setRowCount(int iRows) {
        this.iRows = iRows;
    }

    /**
     * This method checks if a cell is editable or not.
     * @param iRow Specifies the tables row.
     * @param iCol Specifies the tables column.
     * @return Returns true if the cell is editable. Else, false will be returned.
     */
    public boolean isCellEditable(int iRow, int iCol) {
        if (iCol > 1)
            return true;

        return false;
    }

    /**
     * Gets a specific table value
     * @param iRow Specifies the tables row
     * @param iCol Specifies the tables column
     * @return Returns the object at (row,column)
     */
    public Object getValueAt(int iRow, int iCol) {
        CalendarEvent calendarEvent = (CalendarEvent) vecEvents.get(iRow);

        switch (iCol) {
        case 0:
            String sEnum = "" + (iRow + 1);
            while (sEnum.length() < iEnumLength)
                sEnum = "0" + sEnum;
            return sEnum;
        case 1:
            return calendarEvent.getDate();
        case 2:
            return calendarEvent.getDescription();
        case 3:
            return calendarEvent.getCategoryName();
        case 4:
            return calendarEvent.getPlace();
        }

        return "N/A";
    }

    /**
     * This function updates the data of the JTable.
     * @param object Specifies the new value.
     * @param iRow Specifies the row of the cell to be updated.
     * @param iCol Specifies the column of the cell to be updated.
     */
    public void setValueAt(Object object, int iRow, int iCol) {
        CalendarEvent calendarEvent = (CalendarEvent) vecEvents.get(iRow);

        switch (iCol) {
        case 2:
            calendarEvent.setDescription((String) object);
            break;
        case 3:
            calendarEvent.setCategoryName((String) object);
            break;
        case 4:
            calendarEvent.setPlace((String) object);
            break;
        default:
            return;
        }

        runLastSorter();
        jTable.updateUI();

        ClientRequest clientRequest = new ClientRequest(ClientRequest.MODIFY_EVENT);
        clientRequest.setEvent(calendarEvent);
        ServerRequester.sendClientRequest(clientRequest);
    }

    /**
     * This method returns the class types of a specific table column.
     * @param iCol Specifies the column index to get the class type for.
     * @return Returns the class type of the specified column.
     */
    public Class getColumnClass(int iCol) {
        switch (iCol) {
        case 0:
            return String.class;
        case 1:
            return MyDate.class;
        case 2:
            return String.class;
        case 3:
            return String.class;
        case 4:
            return String.class;
        }

        return String.class;
    }

    /**
     * This method returns the column header of a specific table column.
     * @param iCol Specifies the column index to get the column header for.
     * @return Returns the header string of the specified column.
     */
    public String getColumnName(int iCol) {
        switch (iCol) {
        case 0:
            return NUM_HEADER;
        case 1:
            return DATE_HEADER;
        case 2:
            return DESCRIPTION_HEADER;
        case 3:
            return CATEGORY_HEADER;
        case 4:
            return PLACE_HEADER;
        }

        return "N/A";
    }

    /**
     * This method returns the calendar event which belongs to a specific table row
     * @param iRow Specifies the table row index
     * @return Returns the calendar event
     */
    public CalendarEvent getEvent(int iRow) {
        return (CalendarEvent) vecEvents.get(iRow);
    }

    /**
     * This method updates all the value objects (calendar events) of the table.
     * @param vecEvents Specifies the vector of the events to be used.
     */
    public void setEvents(Vector vecEvents) {
        if (vecEvents == null)
            return;

        this.vecEvents = vecEvents;
        setRowCount(vecEvents.size());

        runLastSorter();
    }

    /**
     * This method sorts the data again using the last sorting method being used.
     */
    private void runLastSorter() {
        if (lastSortComparator == null) {
            sortByDate();

        } else {
            Collections.sort(vecEvents, lastSortComparator);
            jTable.updateUI();
        }
    }

    /**
     * Sorts the table content by a specified table column.
     * @param iCol Specifies the table column to sort against.
     */
    public void sortByCol(int iCol) {
        switch (iCol) {
        case 0:
        case 1:
            sortByDate();
            break;
        case 2:
            sortByDescription();
            break;
        case 3:
            sortByCategory();
            break;
        case 4:
            sortByPlace();
            break;
        }
    }

    /**
     * Reverse sorts the table content by a specified table column.
     * @param iCol Specifies the table column to reverse sort against.
     */
    public void reverseSortByCol(int iCol) {
        switch (iCol) {
        case 0:
        case 1:
            reverseSortByDate();
            break;
        case 2:
            reverseSortByDescription();
            break;
        case 3:
            reverseSortByCategory();
            break;
        case 4:
            reverseSortByPlace();
            break;
        }
    }

    /**
     * Sorts the table content by their IDs.
     */
    public void sortByID() {
        Main.statusMessage("Sorting by ID...");

        lastSortComparator = new Comparator() {
            public int compare(Object objectA, Object objectB) {
                int i = ((CalendarEvent) objectA).getID();
                int j = ((CalendarEvent) objectB).getID();
                if (i == j)
                    return 0;
                else if (i < j)
                    return -1;
                else
                    return 1;
            }
            public boolean equals(Object object) {
                return false;
            }
        };
        Collections.sort(vecEvents, lastSortComparator);
        jTable.updateUI();

        Main.statusMessage("Sorting by date done");
    }

    /**
     * Sorts the table content by their dates.
     */
    public void sortByDate() {
        Main.statusMessage("Sorting by date...");

        lastSortComparator = new Comparator() {
            public int compare(Object objectA, Object objectB) {
                long l = ((CalendarEvent) objectA).getDate().getTime();
                long m = ((CalendarEvent) objectB).getDate().getTime();
                if (l == m)
                    return 0;
                else if (l < m)
                    return -1;
                else
                    return 1;
            }
            public boolean equals(Object object) {
                return false;
            }
        };
        Collections.sort(vecEvents, lastSortComparator);
        jTable.updateUI();

        Main.statusMessage("Sorting by date done");
    }

    /**
     * Sorts the table content by their category names.
     */
    public void sortByCategory() {
        Main.statusMessage("Sorting by category...");
        lastSortComparator = new Comparator() {
            public int compare(Object objectA, Object objectB) {
                CalendarEvent eventA = (CalendarEvent) objectA;
                CalendarEvent eventB = (CalendarEvent) objectB;
                return eventA.getCategoryName().compareTo(eventB.getCategoryName());
            }
            public boolean equals(Object object) {
                return false;
            }
        };
        Collections.sort(vecEvents, lastSortComparator);
        jTable.updateUI();
        Main.statusMessage("Sorting by category done");
    }

    /**
     * Sorts the table content by their description texts.
     */
    public void sortByDescription() {
        Main.statusMessage("Sorting by description...");
        lastSortComparator = new Comparator() {
            public int compare(Object objectA, Object objectB) {
                CalendarEvent eventA = (CalendarEvent) objectA;
                CalendarEvent eventB = (CalendarEvent) objectB;
                return eventA.getDescription().compareTo(eventB.getDescription());
            }
            public boolean equals(Object object) {
                return false;
            }
        };
        Collections.sort(vecEvents, lastSortComparator);
        jTable.updateUI();
        Main.statusMessage("Sorting by description done");
    }

    /**
     * Sorts the table content by their places.
     */
    public void sortByPlace() {
        Main.statusMessage("Sorting by place...");
        lastSortComparator = new Comparator() {
            public int compare(Object objectA, Object objectB) {
                CalendarEvent eventA = (CalendarEvent) objectA;
                CalendarEvent eventB = (CalendarEvent) objectB;
                return eventA.getPlace().compareTo(eventB.getPlace());
            }
            public boolean equals(Object object) {
                return false;
            }
        };
        Collections.sort(vecEvents, lastSortComparator);
        jTable.updateUI();
        Main.statusMessage("Sorting by place done");
    }

    /**
     * Reverse sorts the table content by their IDs.
     */
    public void reverseSortByID() {
        Main.statusMessage("Reverse sorting by date...");
        lastSortComparator = new Comparator() {
            public int compare(Object objectA, Object objectB) {
                long i = ((CalendarEvent) objectA).getID();
                long j = ((CalendarEvent) objectB).getID();
                if (i == j)
                    return 0;
                else if (i > j)
                    return -1;
                else
                    return 1;
            }
            public boolean equals(Object object) {
                return false;
            }
        };
        Collections.sort(vecEvents, lastSortComparator);
        jTable.updateUI();
        Main.statusMessage("Reverse sorting by date done");
    }

    /**
     * Reverse sorts the table content by their dates.
     */
    public void reverseSortByDate() {
        Main.statusMessage("Reverse sorting by date...");
        lastSortComparator = new Comparator() {
            public int compare(Object objectA, Object objectB) {
                long l = ((CalendarEvent) objectA).getDate().getTime();
                long m = ((CalendarEvent) objectB).getDate().getTime();
                if (l == m)
                    return 0;
                else if (l > m)
                    return -1;
                else
                    return 1;
            }
            public boolean equals(Object object) {
                return false;
            }
        };
        Collections.sort(vecEvents, lastSortComparator);
        jTable.updateUI();
        Main.statusMessage("Reverse sorting by date done");
    }

    /**
     * Reverse sorts the table content by their category names.
     */
    public void reverseSortByCategory() {
        Main.statusMessage("Reverse sorting by category...");
        lastSortComparator = new Comparator() {
            public int compare(Object objectA, Object objectB) {
                CalendarEvent eventA = (CalendarEvent) objectA;
                CalendarEvent eventB = (CalendarEvent) objectB;
                return eventB.getCategoryName().compareTo(eventA.getCategoryName());
            }
            public boolean equals(Object object) {
                return false;
            }
        };
        Collections.sort(vecEvents, lastSortComparator);
        jTable.updateUI();
        Main.statusMessage("Reverse sorting by category done");
    }

    /**
     * Reverse sorts the table content by their description texts.
     */
    public void reverseSortByDescription() {
        Main.statusMessage("Reverse sorting by description...");
        lastSortComparator = new Comparator() {
            public int compare(Object objectA, Object objectB) {
                CalendarEvent eventA = (CalendarEvent) objectA;
                CalendarEvent eventB = (CalendarEvent) objectB;
                return eventB.getDescription().compareTo(eventA.getDescription());
            }
            public boolean equals(Object object) {
                return false;
            }
        };
        Collections.sort(vecEvents, lastSortComparator);
        jTable.updateUI();
        Main.statusMessage("Reverse sorting by description done!");
    }

    /**
     * Reverse sorts the table content by their places.
     */
    public void reverseSortByPlace() {
        Main.statusMessage("Reverse sorting by place...");
        lastSortComparator = new Comparator() {
            public int compare(Object objectA, Object objectB) {
                CalendarEvent eventA = (CalendarEvent) objectA;
                CalendarEvent eventB = (CalendarEvent) objectB;
                return eventB.getPlace().compareTo(eventA.getPlace());
            }
            public boolean equals(Object object) {
                return false;
            }
        };
        Collections.sort(vecEvents, lastSortComparator);
        jTable.updateUI();
        Main.statusMessage("Reverse sorting by place done");
    }
}
