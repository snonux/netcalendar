/* NetCalendar 2006, 2009 (c) Dipl.-Inform. (FH) Paul C. Buetow
 * http://buetow.org - netcalendar@dev.buetow.org
 */

package shared;

/**
 * This interface specifies a callback routine.
 * @author Paul C. Buetow
 *
 */
public interface DoCallback {
    public void callback(Object o);
}
