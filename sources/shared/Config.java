/* NetCalendar 2006, 2009 (c) Dipl.-Inform. (FH) Paul C. Buetow
 * http://buetow.org - netcalendar@dev.buetow.org
 */

package shared;

import java.util.*;
import java.net.*;
import java.io.*;

/**
 * This class makes all the configuration options available to all the other classes and objects
 * of this program. Its reading and writing back the values from and to the netcalendar.conf file.
 * @author Paul C. Buetow
 *
 */
public final class Config {
    public static final String VERSION = "NetCalendar 1.1 SVN";
    private static final String CONFIG_FILE = "netcalendar.conf";
    private static HashMap mapConfigVals;

    /**
     * Initializes all the config values.
     * @param args The start parameters.
     */
    public static void initialize(String[] args) {
        if (args.length > 0) {
            if (args[0].equals("--help")) {
                System.out.println(Config.VERSION + " help");
                System.out.println("(c) 2006, 2009 by Dipl.-Inform. (FH) Paul C. Buetow [netcalendar@dev.buetow.org]");
                System.out.println("This is free software, see the LICENSE.txt for copying conditions. There is NO");
                System.out.println("warranty; not even for MERCHANTABILITY of FITNESS FOR A PARTICULAR PURPOSE.");
                System.out.println();
                System.out.println("Possible start parameters are:");
                System.out.println("\t--help Shows this help and exists");
                System.out.println("\t--client-only Runs the NetCalendar client only");
                System.out.println("\t--server-address=host Specifies the server host to connect to");
                System.out.println("\t--server-only Runs the NetCalendar server only");
                System.out.println("\t--client-and-server Runs the NetCalendar server and client");
                System.out.println("\t  within the same process!");
                System.out.println();
                System.exit(0);
            }
        }

        mapConfigVals = new HashMap();
        readConfig(CONFIG_FILE);

        for (int i = 0; i < args.length; ++i) {
            if (args[i].equals("--server-only")) {
                mapConfigVals.put("server_run", "true");
                mapConfigVals.put("client_run", "false");

            } else if (args[i].equals("--client-only")) {
                mapConfigVals.put("server_run", "false");
                mapConfigVals.put("client_run", "true");

            } else if (args[i].equals("--client-and-server")) {
                mapConfigVals.put("server_run", "true");
                mapConfigVals.put("client_run", "true");
                mapConfigVals.put("server_address", "localhost");

            } else if (args[i].startsWith("--server-address=")) {
                mapConfigVals.put("server_address", args[i].substring(17));
            }
        }

        if (args.length > 0)
            writeConfigToFile();
    }

    /**
     * This methods reads the options from the calendar config file 'netcalendar.conf'.
     * @param sConfigFile Specifies the relative or absolute path to the config file.
     */
    public static void readConfig(String sConfigFile) {
        Main.infoMessage("Config: Using config file " + sConfigFile);
        File file = new File(sConfigFile);

        if (!file.isFile()) {
            Main.infoMessage("Config: " + sConfigFile + " is not a file!");
            System.exit(1);
        }

        try {
            BufferedReader in = new BufferedReader(new FileReader(file));
            String sLine;

            while ((sLine = in.readLine()) != null) {
                int iSeperator = sLine.indexOf('=');

                if (iSeperator > -1) {
                    int iEnd = sLine.indexOf(';');

                    if (iEnd > -1) {
                        String sKey = sLine.substring(0, iSeperator).trim();
                        String sVal = sLine.substring(iSeperator+1, iEnd).trim();
                        mapConfigVals.put(sKey, sVal);
                    }
                }
            }

        } catch (Exception e) {
            Main.infoMessage("Config: " + e.getMessage());
            System.exit(1);
        }
    }

    /**
     * This method returns the inet address of the calendar server.
     * @return Returns the calendar server's address .
     */
    public static InetAddress getServerAddress() {
        InetAddress serverAddress = null;

        try {
            serverAddress = InetAddress.getByName(Config.getStringValue("server_address"));
        } catch (Exception e) {
            Main.infoMessage("Server address error: " + e.getMessage());
        }

        return serverAddress;
    }

    /**
     * This method returns a specific string config value.
     * @param sKey Specifies the key of the config value to return.
     * @return Returns the requested config value.
     */
    public static String getStringValue(String sKey) {
        return getStringValue(sKey, true);
    }

    /**
     * This method returns a specific string config value.
     * @param sKey Specifies the key of the config value to return.
     * @param bLogMessages Specifies if this action should be logged or not.
     * @return Returns the requested config value.
     */
    public static String getStringValue(String sKey, boolean bLogMessage) {
        String sRet = (String) mapConfigVals.get(sKey);

        if (sRet != null)
            sRet = sRet.trim();

        return sRet;
    }

    /**
     * This method returns a specific integer config value.
     * @param sKey Specifies the key of the config value to return.
     * @return Returns the requested config value.
     */
    public static int getIntValue(String sKey) {
        String sVal = (String) mapConfigVals.get(sKey);

        if (sVal != null)
            sVal = sVal.trim();

        return Integer.parseInt(sVal);
    }

    /**
     * This method returns a specific boolean config value.
     * @param sKey Specifies the key of the config value to return.
     * @return Returns the requested config value.
     */
    public static boolean getBooleanValue(String sKey) {
        String sVal = (String) mapConfigVals.get(sKey);

        if (sVal != null)
            sVal = sVal.trim();

        boolean bRet = false;

        if (sVal != null)
            bRet = sVal.equals("true");

        return bRet;
    }

    /**
     * This method returns a set of keys of the config hash map.
     * @return Returns a Set object containing all keys of the Config's hash map.
     */
    public static Set getKeySet() {
        return mapConfigVals.keySet();
    }

    /**
     * This method changes or adds a specific config value.
     * @param sKey Specifies the config key to set or update.
     * @param sVal Specifies the value of the key to set or update.
     */
    public static void setValue(String sKey, String sVal) {
        mapConfigVals.put(sKey, sVal);
    }

    /**
     * This method returns a sorted String array of all available config keys.
     * @return Returns a sorted String array of all available config keys.
     */
    public static String [] getSortedKeyArray() {
        Set keySet = getKeySet();
        Object [] keys = keySet.toArray();
        int iNumPairs = keys.length;
        String [] sKeys = new String[iNumPairs];

        for (int i = 0; i < iNumPairs; ++i)
            sKeys[i] = (String) keys[i];

        Arrays.sort(sKeys);

        return sKeys;
    }

    /**
     * This method writes the current configuration to the config file "netcalendar.conf".
     */
    public static void writeConfigToFile() {
        String [] sKeys = getSortedKeyArray();
        int iNumKeys = sKeys.length;

        try {
            FileWriter fileWriter = new FileWriter(CONFIG_FILE);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            for (int i = 0; i < iNumKeys; ++i) {
                String sKey = sKeys[i];
                String sVal = getStringValue(sKey);
                bufferedWriter.write(sKey + "=" + sVal + ";\n");
            }

            bufferedWriter.close();

        } catch (IOException e) {
            Main.infoMessage("Error: " + e.toString());
        }
    }
}
