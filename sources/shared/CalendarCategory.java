/* NetCalendar 2006, 2009 (c) Dipl.-Inform. (FH) Paul C. Buetow
 * http://buetow.org - netcalendar@dev.buetow.org
 */

/**
 *
 */
package shared;

import java.util.Enumeration;
import java.util.Vector;
import java.util.regex.*;
import java.io.*;

import shared.remotecall.*;


/**
 * This class specifies a category of the calendar events. Its wrapping a vector of references
 * of all events of this category.
 * @author Paul C. Buetow
 *
 */
public class CalendarCategory {
    private Vector vecEvents;
    private String sCategoryName;
    private boolean bHasChanged = false;
    private File categoryFile;

    /**
     * Creates a new calendar category object and sets the categorie's name to the filename of the category.
     * @param categoryFile The file object which contains the unparsed events for this specific category. It will be unset after parsing.
     */
    public CalendarCategory(File categoryFile) {
        this.categoryFile = categoryFile;
        this.sCategoryName = categoryFile.getName();

        // sCategoryName is now "calendar.Name", remove now the leading 'calendar.'
        sCategoryName = sCategoryName.substring(sCategoryName.indexOf('.') + 1);
    }

    /**
     * Creates a new calendar category object and sets the categorie's File to the name of the category.
     * @param sCategoryName Specifies the categorie's name.
     */
    public CalendarCategory(String sCategoryName) {
        this.sCategoryName = sCategoryName;
    }

    /**
     * This method sets the categorie's initial events.
     * @param vecEvents Specifies the categorie's events.
     */
    public void setEvents(Vector vecEvents) {
        this.vecEvents = vecEvents;
    }

    /**
     * This method adds an event to the category.
     * @param event Specifies the calendar event to add to this category.
     */
    public void addEvent(CalendarEvent event) {
        if (vecEvents == null)
            vecEvents = new Vector();

        vecEvents.add(event);
    }

    /**
     * This method returns the calendar events file object.
     * @return Returns the categorie's file object.
     */
    public File getFile() {
        return categoryFile;
    }

    /**
     * This method Sets the categories file handle to null. This is done if all the data has been read from
     * the file database.
     */
    public void unsetFile() {
        categoryFile = null;
    }

    /**
     * This method returns the categorie's name.
     * @return Returns the categorie's name.
     */
    public String getName() {
        return sCategoryName;
    }

    /**
     * This method sets the categorie's name.
     */
    public void setName(String sCategoryName) {
        if (this.sCategoryName.equals(sCategoryName))
            return;

        deleteDatabaseFile();
        this.sCategoryName = sCategoryName;
        categoryFile = new File(Config.getStringValue("server_database_dir")+"/calendar."+sCategoryName);
        setHasChanged(true);
    }

    /**
     * This method deletes the database file of this event.
     */
    public void deleteDatabaseFile() {
        if (categoryFile == null)
            categoryFile = new File(Config.getStringValue("server_database_dir")+"/calendar."+getName());

        Main.infoMessage("Removing " + categoryFile.getPath());
        categoryFile.delete();
    }

    /**
     * This method returns the categorie's events.
     * @return Returns a Vector of all CalendarEvent objects.
     */
    public Vector getEvents() {
        return vecEvents;
    }

    /**
     * This method looks for all calendar events of this category which matches a given client's request.
     * @param clientRequest Specifies the client request sent by the calendar client.
     * @return Returns a Vector of all matching CalendarEvent objects.
     */
    public Vector getMatchingEvents(ClientRequest clientRequest) {
        Vector vecMatching = new Vector();
        int iNumEventsToRequest = clientRequest.getNumEventsToRequest();

        Enumeration enumEvents = vecEvents.elements();
        while (enumEvents.hasMoreElements()) {
            CalendarEvent event = (CalendarEvent) enumEvents.nextElement();
            if (clientRequest.match(event))
                vecMatching.add(event);

            if (iNumEventsToRequest > -1 && iNumEventsToRequest == vecMatching.size())
                break;
        }

        return vecMatching;
    }

    /**
     * This method checks the current category's name against a regular expression pattern.
     * @param pattern Specifies the pattern to be used.
     * @return Returns true if the event matches, else false will be returned.
     */
    public boolean matchesName(Pattern pattern) {
        if (pattern == null)
            return false;

        return pattern.matcher(getName()).find();
    }

    /**
     * This method removes a given event from its event vector.
     * @param event Specifies the calendar event object to be removed from this category
     */
    public void removeEvent(CalendarEvent event) {
        getEvents().removeElement(event);
    }

    /**
     * This method lets you check if the calendar category has been modified/changed by a
     * calendar client request. This is needed by the CalendarDatabase.flush method, to write all
     * the changes back to the database files.
     * @return Returns true if the category has changed, else it will return false.
     */
    public boolean hasChanged() {
        return bHasChanged;
    }

    /**
     * This method merges two categories.
     * @param calendarCategory Specifies the category to merge with.
     */
    public void merge(CalendarCategory calendarCategory) {
        Enumeration enumEvents = calendarCategory.getEvents().elements();
        while (enumEvents.hasMoreElements())
            vecEvents.add(enumEvents.nextElement());

        System.out.println(vecEvents);
    }

    /**
     * This method specifies if the calendar category has been modified/changed or not.
     * @param bHasChanged Specifies if the category has been modified/changed or not.
     */
    public void setHasChanged(boolean bHasChanged) {
        this.bHasChanged = bHasChanged;
    }

    /**
     * This method flushes the calendar category to the database at the filesystem.
     */
    public void flush() {
        Main.infoMessage("Flushing category " + getName() + " to database!");

        if (vecEvents.size() == 0) {
            deleteDatabaseFile();
            return;
        }

        try {
            FileWriter fileWriter = new FileWriter(
                Config.getStringValue("server_database_dir") + "/calendar." + getName());
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            Enumeration enumEvent = vecEvents.elements();
            while (enumEvent.hasMoreElements()) {
                CalendarEvent event = (CalendarEvent) enumEvent.nextElement();
                event.flush(bufferedWriter);
            }

            bufferedWriter.close();

        } catch (IOException e) {
            Main.infoMessage("Error: Database flush error: " + e.getMessage());
        }

        setHasChanged(false);
    }
}
