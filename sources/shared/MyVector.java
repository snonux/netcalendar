/* NetCalendar 2006, 2009 (c) Dipl.-Inform. (FH) Paul C. Buetow
 * http://buetow.org - netcalendar@dev.buetow.org
 */

package shared;

import java.util.Enumeration;
import java.util.Vector;

/**
 * This class extends the standard Vector class and defined additional help methods.
 * @author Paul C. Buetow
 *
 */
public class MyVector extends Vector {
    private static final long serialVersionUID = 1L;

    /**
     * This method appends a vector at the end.
     * @param vecAppend Specifies the vector to be appended.
     */
    public void appendVector(Vector vecAppend) {
        Enumeration enumAppend = vecAppend.elements();
        while (enumAppend.hasMoreElements())
            this.add(enumAppend.nextElement());
    }
}
