/* NetCalendar 2006, 2009 (c) Dipl.-Inform. (FH) Paul C. Buetow
 * http://buetow.org - netcalendar@dev.buetow.org
 */

/**
 *
 */
package shared;

import java.util.*;
import java.util.regex.*;
import java.io.*;

/**
 * This class specifies calendar events. Calendar event objects are the most used objects of the
 * whole netcalendar suite.
 * @author Paul C. Buetow
 *
 */
public class CalendarEvent implements Serializable {
    private static final long serialVersionUID = 1L;
    private MyDate date;
    private CalendarCategory category;
    private String sCategoryName;
    private String sDescription;
    private String sPlace;
    private boolean bYearly;
    private int iEventID;

    static private int iEventIDCount = 0;

    /**
     * Simple constructor, creates an event object and increments iEventIDCount by one.
     * @param category Specifies the event's category.
     */
    public CalendarEvent(CalendarCategory category) {
        setCategory(category);
        setEventID(iEventIDCount++);
        setYearly(false);
    }

    /**
     * Simple constructor, creates an event object and increments iEventIDCount by one.
     * @param sCategoryName Specifies the event's category name.
     */
    public CalendarEvent(String sCategoryName) {
        setCategoryName(sCategoryName.trim());
        setEventID(iEventIDCount++);
        setYearly(false);
    }

    /**
     * This method sets the event's date.
     * @param date Specifies the event's date.
     */
    public void setDate(Date date) {
        if (date instanceof MyDate)
            this.date = (MyDate) date;
        else
            this.date = new MyDate(date);
    }

    /**
     * This method sets the event's category.
     * @param category Specifies the event's category.
     */
    public void setCategory(CalendarCategory category) {
        this.category = category;
        setCategoryName(category.getName());
    }

    /**
     * This event returns the event's calendar category.
     * @return Returns the event's calenda category.
     */
    public CalendarCategory getCategory() {
        return category;
    }

    /**
     * This methos sets thhe event's category name.
     * @param sCategoryName Specifies the event's category name.
     */
    public void setCategoryName(String sCategoryName) {
        this.sCategoryName = sCategoryName.trim();
    }

    /**
     * This method sets the event's descriptions tring.
     * @param sDescription Specifies the event's description string.
     */
    public void setDescription(String sDescription) {
        this.sDescription = sDescription.trim();
    }

    /**
     * This method sets the event's place string.
     * @param sPlace Specifies the event's place string.
     */
    public void setPlace(String sPlace) {
        this.sPlace = sPlace.trim();
    }

    /**
     * This method sets the event's ID number.
     * @param iEventID specifies the event's ID number.
     */
    public void setEventID(int iEventID) {
        this.iEventID = iEventID;
    }

    /**
     * This method specifies if this event occurs yearly or not.
     * @param bYearly Specifies if this event occury yearly or not.
     */
    public void setYearly(boolean bYearly) {
        this.bYearly = bYearly;
    }

    /**
     * This method returns the event's category name.
     * @return Returns the event's category name.
     */
    public String getCategoryName() {
        return sCategoryName;
    }

    /**
     * This method returns the event's description string.
     * @return Returns the event's description string.
     */
    public String getDescription() {
        return sDescription;
    }

    /**
     * This method returns the event's place string.
     * @return Returns the event's place string.
     */
    public String getPlace() {
        return sPlace;
    }

    /**
     * This method returns the event's ID.
     * @return Returns the event's ID.
     */
    public int getID() {
        return iEventID;
    }

    /**
     * This method returns the event's date object.
     * @return Returns the event's date object.
     */
    public MyDate getDate() {
        return date;
    }

    /**
     * This method returns the event's ID number.
     * @return Returns the event's ID number.
     */
    public int getEventID() {
        return iEventID;
    }

    /**
     * This method checks if this event occurs yearly or not.
     * @return Returns true if the event occurs yearly, else it returns false.
     */
    public boolean isYearly() {
        return bYearly;
    }

    /**
     * Checks if the current event matches all available data of the event agains
     * a given regular expression pattern.
     * @param pattern Specifies the pattern to match against.
     * @return Returns true if one element of the event matches.
     */
    public boolean matches(Pattern pattern) {
        if (pattern == null)
            return false;

        else if (matchesDescription(pattern))
            return true;

        else if (matchesPlace(pattern))
            return true;

        else if (matchesCategoryName(pattern))
            return true;

        return matchesDateString(pattern);
    }

    /**
     * Checks if the current event's name matches agains a regular expression pattern.
     * @param pattern Specifies the pattern to be matched against.
     * @return Returns true if the event matches, else false will be returned.  If the pattern is null, true will be returned.
     */
    public boolean matchesCategoryName(Pattern pattern) {
        if (pattern == null)
            return true;

        return pattern.matcher(getCategoryName()).find();
    }

    /**
     * Checks if the current event's description string matches agains a regular expression pattern.
     * @param pattern Specifies the pattern to match against.
     * @return Returns true if the event matches, else false will be returned. If the pattern is null, true will be returned.
     */
    public boolean matchesDescription(Pattern pattern) {
        if (pattern == null)
            return true;

        return pattern.matcher(getDescription()).find();
    }

    /**
     * Checks if the current event's place string matches agains a regular expression pattern.
     * @param pattern Specifies the pattern to match against.
     * @return Returns true if the event matches, else false will be returned. If the pattern is null, true will be returned.
     */
    public boolean matchesPlace(Pattern pattern) {
        if (pattern == null)
            return true;

        return pattern.matcher(getPlace()).find();
    }

    /**
     * Checks if the current event's category name matches agains a regular expression pattern
     * @param pattern Specifies the pattern to match against.
     * @return Returns true if the event matches, else false will be returned. If the pattern is null, true will be returned.
     */
    public boolean matchesCategory(Pattern pattern) {
        if (pattern == null)
            return true;

        return pattern.matcher(getCategoryName()).find();
    }

    /**
     * Checks if the current event's date string matches agains a regular expression pattern
     * @param pattern Specifies the pattern to match against.
     * @return Returns true if the event matches, else false will be returned. If the pattern is null, true will be returned.
     */
    public boolean matchesDateString(Pattern pattern) {
        if (pattern == null)
            return true;

        return pattern.matcher(getDate().toString()).find();
    }

    /**
     * Needed for ojbect serialization (sending part)-
     * @param objectOutputStream Specifies the output stream to use.
     * @throws IOException
     */
    private void writeObject(ObjectOutputStream objectOutputStream)
    throws IOException {
        objectOutputStream.writeInt(iEventID);
        objectOutputStream.writeBoolean(isYearly());
        objectOutputStream.writeObject(getDate());
        objectOutputStream.writeObject(getCategoryName());
        objectOutputStream.writeObject(getDescription());
        objectOutputStream.writeObject(getPlace());
    }

    /**
     * Needed for object serialization (receiving part).
     * @param objectInputStream Specifies the input stream to use.
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void readObject(ObjectInputStream objectInputStream)
    throws IOException, ClassNotFoundException {
        setEventID(objectInputStream.readInt());
        setYearly(objectInputStream.readBoolean());
        setDate((Date) objectInputStream.readObject());
        setCategoryName((String) objectInputStream.readObject());
        setDescription((String) objectInputStream.readObject());
        setPlace((String) objectInputStream.readObject());

        if (getEventID() < 0 )
            setEventID(iEventIDCount++);
    }

    /**
     * Removes the calendar event from its current category and sets the
     * category reference to null.
     */
    public void removeFromCurrentCategory() {
        CalendarCategory category = getCategory();
        category.removeEvent(this);
    }

    /**
     * This method flushes the calendar event to the database at the filesystem.
     * @param bufferedWriter Specifies the buffered writer to write to.
     */
    protected void flush(BufferedWriter bufferedWriter) throws IOException {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(getDate());
        int iYear = calendar.get(Calendar.YEAR);
        // if (iYear < 11)	iYear += 2000;
        int iMonth = calendar.get(Calendar.MONTH) + 1;
        int iDays = calendar.get(Calendar.DAY_OF_MONTH);
        int iHours= calendar.get(Calendar.HOUR);
        int iMinutes = calendar.get(Calendar.MINUTE);

        String sPlace = getPlace();
        if (!sPlace.equals(""))
            sPlace = ";;" + sPlace;

        String sYearly = isYearly() ? "yearly" : MyDate.addZerosToFront(iYear, 4);
        String sEventLine =
            MyDate.addZerosToFront(iMonth, 2) + "/"
            + MyDate.addZerosToFront(iDays, 2) + "\t"
            + sYearly + "-"
            + MyDate.addZerosToFront(iHours, 2) + ":"
            + MyDate.addZerosToFront(iMinutes, 2) + " "
            + getDescription()
            + sPlace + "\n";

        bufferedWriter.write(sEventLine);
    }

    /**
     * This method is needed for a text representation of the object.
     * @return Returns object represented as a String.
     */
    public String toString() {
        return "#" + getEventID() + " " + getDate() + "\n"
               + "Yearly     : " + isYearly() + "\n"
               + "Category   : " + getCategoryName() + "\n"
               + "Description: " + getDescription() + "\n"
               + "Place      : " + getPlace() + "\n";
    }
}
