/* NetCalendar 2006, 2009 (c) Dipl.-Inform. (FH) Paul C. Buetow
 * http://buetow.org - netcalendar@dev.buetow.org
 */

package shared;

import java.util.*;

/**
 * This class extends the standard Date class and customizes the toString method.
 * This class also contains some helper methods to make the life easier.
 * @author Paul C. Buetow
 *
 */
public class MyDate extends Date {
    private final static long serialVersionUID = 1L;
    private final static String [] DAYS = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };
    //private Date date;

    /**
     * Copy constructor.
     * @param data Specifies the date objekt to copy the time from.
     */
    public MyDate(Date date) {
        super(date.getTime());
    }

    /**
     * Copy constructor.
     * @param calendar Specifies the date objekt to copy the time from.
     */
    /*
    public MyDate(Calendar calendar) {
        super(calendar.getTime());
    }
    */

    /**
     * @see java.lang.Object#toString()
     */
    public String toString() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(this);

        String sYear = addZerosToFront(calendar.get(Calendar.YEAR), 4);
        String sMonth = addZerosToFront(calendar.get(Calendar.MONTH) + 1, 2);
        String sDays = addZerosToFront(calendar.get(Calendar.DAY_OF_MONTH), 2);
        String sHours= addZerosToFront(calendar.get(Calendar.HOUR), 2);
        String sMinutes = addZerosToFront(calendar.get(Calendar.MINUTE), 2);
        String sDay = DAYS[calendar.get(Calendar.DAY_OF_WEEK)-1];

        return sDays + "." + sMonth + "." + sYear + " " + sHours + ":" + sMinutes + " " + sDay;
    }

    /**
     * This method adds zeros in front of a number and returns it as a string.
     * @param iValue Specifies the number to add zeros to.
     * @param iWantedLength Specifies the wanted max length of the string.
     * @return Returns the number represented as a string with additional zeros on front.
     */
    public static String addZerosToFront(int iValue, int iWantedLength) {
        String sValue = iValue + "";

        while (sValue.length() < iWantedLength)
            sValue = "0" + sValue;

        return sValue;
    }
}
