/* NetCalendar 2006, 2009 (c) Dipl.-Inform. (FH) Paul C. Buetow
 * http://buetow.org - netcalendar@dev.buetow.org
 */

package shared;

import java.io.*;
import javax.swing.JFrame;
import server.*;
import client.*;

/**
 * This class contains the static main method to start the program from.
 * This class is also responsible for handling info ans status messages and
 * the logging option of those messages.
 * @author Paul C. Buetow
 *
 */
public final class Main {
    private static NetCalendarClient netCalendarClient = null;
    private static BufferedWriter bufferedLogfileWriter = null;

    /**
     * This method is the start point of the whole program. Its initializing the static Config object
     * and starts a calendar server and/or a calendar client thread depending on the "server_run" and
     * "client_run" configuration options.
     * @param args Specifies the program arguments (not used yet by the program)
     */
    public static void main(String[] args) {
        Config.initialize(args);

        try {
            FileWriter fileWriter = new FileWriter(Config.getStringValue("logfile"));
            bufferedLogfileWriter = new BufferedWriter(fileWriter);

        } catch (IOException e) {
            infoMessage("Error: Could not open logfile: " + e.getMessage());
        }


        if (Config.getBooleanValue("server_run")) {
            new NetCalendarServer(
                Config.getIntValue("server_port"),
                checkDatabaseDir(Config.getStringValue("server_database_dir")));
        }

        if (Config.getBooleanValue("client_run")) {
            new Thread(new SplashScreen()).start();
            //Make sure we have nice window decorations.
            JFrame.setDefaultLookAndFeelDecorated(true);
            netCalendarClient = new NetCalendarClient();
        }
    }

    /**
     * For checking if the database dir exists. If not, use one upper level
     * directory.
     * @param sDatabaseDir The database dir
     * @return The database dir if exists, else one upper level directory
     */
    private static String checkDatabaseDir(String sDatabaseDir) {
        if (new File(sDatabaseDir).exists())
            return sDatabaseDir;

        return "../" + sDatabaseDir;
    }

    /**
     * All info messages of the calendar client and the calendar server go through this method.
     * They are not using System.out.* directly.
     * @param sMessage Specifies the program info message.
     */
    public static void infoMessage(String sMessage) {
        // Later: Add logging to a logfile!
        System.out.println(sMessage);
        logMessage(sMessage);
    }

    /**
     * This method is for various messages. All messages will show up in the status bar of the client's main window.
     * If there is no main window, the infoMessage method will be used instead.
     * @param sMessage Specifies the message to be displayed in the status bar.
     */
    public static void statusMessage(String sMessage) {
        if (netCalendarClient != null)
            netCalendarClient.statusMessage(sMessage);
        else
            infoMessage(sMessage);
    }

    /**
     * This method writes a specific message string into a logfile which is specified in the netcalendar.conf.
     * @param sMessage Specified the message string to write into the logfile.
     */
    private static void logMessage(String sMessage) {
        if (bufferedLogfileWriter != null) {
            try {
                bufferedLogfileWriter.write(sMessage + "\n");
                // bufferedLogfileWriter.flush();

            } catch (IOException e) {
                Main.infoMessage("Error: Could not write to logfile: " + e.toString());
            }
        }
    }

    /**
     * This method is called thenever the server or the client process wants to exit.
     * @param iCode Specifies the exit code to use for the System.exit call.
     */
    public static void exit(int iCode) {
        infoMessage("Shutting down the current process");

        try {
            bufferedLogfileWriter.close();
        } catch (IOException e) {
            System.err.println("Error: Could not close logfile: " + e.toString());
        }

        System.exit(iCode);
    }


    /**
     * This method executes an external program.
     * @param sCommand Specifies the command string to execute.
     */
    public static void execExternalCommand(String sCommand) {
        if (sCommand.equals("none") || sCommand.equals(""))
            return;

        try {
            Runtime.getRuntime().exec(sCommand);

        } catch (IOException e) {
            Main.infoMessage("Error while executing external command: " + e.getMessage());
        }
    }
}
