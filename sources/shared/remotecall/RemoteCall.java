/* NetCalendar 2006, 2009 (c) Dipl.-Inform. (FH) Paul C. Buetow
 * http://buetow.org - netcalendar@dev.buetow.org
 */

package shared.remotecall;

import java.io.*;

/**
 * This is the abstract base class of all other classes of the shared.remotecall package.
 * Its defining some common methods.
 * @author Paul C. Buetow
 *
 */
public abstract class RemoteCall {
    /**
     * This is a help method for writeObject of the child classes, needed for ojbect serialization (sending part).
     * It checks if object is defined. If yes, it will be written to the given object output stream with a leading
     * flag with the value true. Otherwise only the false flag will be written to the object output stream.
     * @param objectOutputStream Specifies the output stream.
     * @throws IOException
     */
    protected final void writeObjectIfDefined(ObjectOutputStream objectOutputStream, Object object)
    throws IOException {
        if (object == null) {
            objectOutputStream.writeBoolean(false);

        } else {
            objectOutputStream.writeBoolean(true);
            objectOutputStream.writeObject(object);
        }
    }
}
