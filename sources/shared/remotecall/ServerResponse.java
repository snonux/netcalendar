/* NetCalendar 2006, 2009 (c) Dipl.-Inform. (FH) Paul C. Buetow
 * http://buetow.org - netcalendar@dev.buetow.org
 */

package shared.remotecall;

import java.io.*;
import java.util.Vector;

/**
 * This class represents a server response. A server response will sent from the calendar
 * server to the calendar client if the client has sent a client request.
 * @author Paul C. Buetow
 *
 */
public final class ServerResponse extends RemoteCall implements Serializable {
    private static final long serialVersionUID = 1L;
    private Vector vecEvents = null;

    /**
     * Simple constructor, creates a server response which can be sent to the calendar client.
     * @param vecEvents Specifies all calendar events to be sent to the calendar client.
     */
    public ServerResponse(Vector vecEvents) {
        this.vecEvents = vecEvents;
    }

    /**
     * Needed for ojbect serialization (sending part).
     * @param objectOutputStream Specifies the output stream.
     * @throws IOException
     */
    private void writeObject(ObjectOutputStream objectOutputStream)
    throws IOException {
        super.writeObjectIfDefined(objectOutputStream, vecEvents);
    }

    /**
     * Needed for object serialization (recieving part).
     * @param objectInputStream Specifies the input stream.
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void readObject(ObjectInputStream objectInputStream)
    throws IOException, ClassNotFoundException {
        if (objectInputStream.readBoolean())
            vecEvents = (Vector) objectInputStream.readObject();
    }

    /**
     * This method returns a vector of all requested calendar events.
     * @return Returns a Vector of all requested CalendarEvent objects.
     */
    public Vector getEvents() {
        return vecEvents;
    }
}
