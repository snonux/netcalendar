/* NetCalendar 2006, 2009 (c) Dipl.-Inform. (FH) Paul C. Buetow
 * http://buetow.org - netcalendar@dev.buetow.org
 */

package shared.remotecall;

import java.util.*;
import java.io.*;
import java.util.regex.*;

import shared.*;

/**
 * An object of this class is sent from the calendar client to the calendar server each time
 * a request is made. This class encapsulates all the needed information so that the server can
 * response with a serialized server response object.
 * @author Paul C. Buetow
 *
 */
public final class ClientRequest extends RemoteCall implements Serializable {
    private static final long serialVersionUID = 6L;
    private static final Pattern emptyPattern = Pattern.compile("[^\\s]");

    public static final int REQUEST_EVENTS = 0;
    public static final int REQUEST_ALL_EVENTS = 1;
    public static final int MODIFY_EVENT = 2;
    public static final int DELETE_EVENT = 3;
    public static final int ADD_EVENT = 4;
    public static final int RELOAD_DATABASE = 5;
    public static final int FLUSH_DATABASE = 6;
    public static final int SHUTDOWN_SERVER = 7;
    public static final int RENAME_CATEGORY = 8;
    public static final int DELETE_CATEGORY = 9;

    private int iAction = 0;
    private int iNumEventsToRequest;

    private boolean bMainRegexp;
    private boolean bCaseInsensitive;

    private Pattern patAll = null;
    private Pattern patCategory = null;
    private Pattern patDescription = null;
    private Pattern patPlace = null;
    private Pattern patDate = null;
    private Date dateRangeFrom = null;
    private Date dateRangeTo = null;
    private CalendarEvent event = null;
    private String sPassphrase;
    private String sString = null;

    /**
     * Simple constructor, creates a client request object requesting events up
     * from the current date.
     */
    public ClientRequest() {
        dateRangeFrom = new Date();
        initialize(REQUEST_EVENTS);
    }

    /**
     * Simple constructor, creates a client request object requesting events up
     * from the current date.
     * @param iAction Specifies the request type of this request.
     */
    public ClientRequest(int iAction) {
        dateRangeFrom = new Date();
        initialize(iAction);
    }

    /**
     * This method initializes some common stuff. Called by each constructor.
     * @param iAction Specifies the request type of this request.
     */
    private void initialize(int iAction) {
        this.iAction = iAction;
        this.sPassphrase = Config.getStringValue("passphrase", false);
        this.bCaseInsensitive = Config.getBooleanValue("regexp_case_insensitive");
        this.iNumEventsToRequest = Config.getIntValue("client_max_events");
    }

    /**
     * This method is needed by the clanedar server to get the type of action requested.
     * @param iAction Specifies the action to test against.
     * @return Returns true if the specified action ID matches with the action ID of this request object.
     */
    public boolean actionIs(int iAction) {
        return this.iAction == iAction;
    }

    /**
     * This method is needed by the calendar server to get the type of action requested.
     * @return Returns true if this object requests new events from the server.
     */
    public boolean requestsNewEvents() {
        return this.iAction < ClientRequest.MODIFY_EVENT;
    }

    /**
     * This method sets the date range to request.
     * @param dateRangeFrom Specifies the beginning date.
     * @param dateRangeTo Specifies the ending date.
     */
    public void setDateRange(Date dateRangeFrom, Date dateRangeTo) {
        this.dateRangeFrom = dateRangeFrom;
        this.dateRangeTo = dateRangeTo;
    }

    /**
     * This method sets a calendar event object to be transfered to the calendar server because the event has been modified.
     * @param event Specifies an event to be transfered to the calendar server.
     */
    public void setEvent(CalendarEvent event) {
        this.event = event;
        if (actionIs(ADD_EVENT))
            event.setEventID(-1);
    }

    /**
     * This method returns the event object.
     * @return Returns the calendar event which is stored inside this client request.
     */
    public CalendarEvent getEvent() {
        return event;
    }

    /**
     * With this method the client can define the maximum number of events to request.
     * @param iNumEvents Specifies the maximum number of events to request.
     */
    public void setNumEventsToRequest(int iNumEvents) {
        this.iNumEventsToRequest = iNumEvents;
    }

    /**
     * This method returns the maximum number of events the client wants to request.
     * @return Returns the maximum number of events the client wants to request.
     */
    public int getNumEventsToRequest() {
        return iNumEventsToRequest;
    }

    /**
     * This method sets if the client contains a regexp from the main session window.
     * @param bMainRegexp Set to true if the user used the main regexp field of the main GUI window.
     */
    public void setMainRegexp(boolean bMainRegexp) {
        this.bMainRegexp = bMainRegexp;
    }

    /**
     * This method compiles a specific pattern object to match with on the server part after serialization.
     * @param sRegexp Compiles a Pattern object with sRegexp as the regular expression.
     */
    public void setRegexpAll(String sRegexp) {
        if (isEmptyPattern(sRegexp))
            return;

        if (bCaseInsensitive)
            patAll = Pattern.compile(sRegexp, Pattern.CASE_INSENSITIVE);
        else
            patAll = Pattern.compile(sRegexp);
    }

    /**
     * This method compiles a specific pattern object to match with on the server part after serialization.
     * @param sRegexp Compiles a Pattern object with sRegexp as the regular expression.
     */
    public void setRegexpCategory(String sRegexp) {
        if (isEmptyPattern(sRegexp))
            return;

        if (bCaseInsensitive)
            patCategory = Pattern.compile(sRegexp, Pattern.CASE_INSENSITIVE);
        else
            patCategory = Pattern.compile(sRegexp);
    }

    /**
     * This method compiles a specific pattern object to match with on the server part after serialization.
     * @param sRegexp Compiles a Pattern object with sRegexp as the regular expression.
     */
    public void setRegexpDescription(String sRegexp) {
        if (isEmptyPattern(sRegexp))
            return;

        if (bCaseInsensitive)
            patDescription = Pattern.compile(sRegexp, Pattern.CASE_INSENSITIVE);
        else
            patDescription = Pattern.compile(sRegexp);
    }

    /**
     * This method sets a string to be transfered to the netcalendar server.
     * @param sString specifies the string to be transfered.
     */
    public void setString(String sString) {
        this.sString = sString;
    }

    /**
     * This method returns a string transfered by the netcalendar client. If no string has been transfered,
     * null will be returned.
     * @return Returns a string transfered by the netcalendar client.
     */
    public String getString() {
        return sString;
    }

    /**
     * This method compiles a specific pattern object to match with on the server part after serialization.
     * @param sRegexp Compiles a Pattern object with sRegexp as the regular expression.
     */
    public void setRegexpPlace(String sRegexp) {
        if (isEmptyPattern(sRegexp))
            return;

        if (bCaseInsensitive)
            patPlace = Pattern.compile(sRegexp, Pattern.CASE_INSENSITIVE);
        else
            patPlace = Pattern.compile(sRegexp);
    }

    /**
     * This method compiles a specific pattern object to match with on the server part after serialization.
     * @param sRegexp Compiles a Pattern object with sRegexp as the regular expression.
     */
    public void setRegexpDate(String sRegexp) {
        if (isEmptyPattern(sRegexp))
            return;

        if (bCaseInsensitive)
            patDate = Pattern.compile(sRegexp, Pattern.CASE_INSENSITIVE);
        else
            patDate = Pattern.compile(sRegexp);
    }

    /**
     * Checks if a specific calendar event matches this client request.
     * @param calendarEvent Specifies the event to match against.
     * @return Returns true if the given event matched. Else, false will be returned.
     */
    public boolean match(CalendarEvent calendarEvent) {
        // If we want to get all events!
        if (actionIs(REQUEST_ALL_EVENTS))
            return true;

        // If we use the regex field of the main GUI
        if (bMainRegexp)
            return calendarEvent.matches(patAll);

        // If we use advanced searching:
        if (dateRangeFrom != null)
            if (dateRangeFrom.getTime() > calendarEvent.getDate().getTime())
                return false;

        if (dateRangeTo != null)
            if (dateRangeTo.getTime() < calendarEvent.getDate().getTime())
                return false;

        if (calendarEvent.matches(patAll) ||
                (calendarEvent.matchesDescription(patDescription)
                 && calendarEvent.matchesCategory(patCategory)
                 && calendarEvent.matchesPlace(patPlace)
                 && calendarEvent.matchesDateString(patDate)))
            return true;

        return false;
    }

    /**
     * Needed for ojbect serialization (sending part).
     * @param objectOutputStream Specifies the output stream to use.
     * @throws IOException
     */
    private void writeObject(ObjectOutputStream objectOutputStream)
    throws IOException {
        objectOutputStream.writeInt(iAction);
        objectOutputStream.writeInt(iNumEventsToRequest);
        objectOutputStream.writeObject(sPassphrase);
        objectOutputStream.writeBoolean(bMainRegexp);
        objectOutputStream.writeBoolean(bCaseInsensitive);

        super.writeObjectIfDefined(objectOutputStream, sString);
        super.writeObjectIfDefined(objectOutputStream, patAll);
        super.writeObjectIfDefined(objectOutputStream, patCategory);
        super.writeObjectIfDefined(objectOutputStream, patDescription);
        super.writeObjectIfDefined(objectOutputStream, patPlace);
        super.writeObjectIfDefined(objectOutputStream, patDate);
        super.writeObjectIfDefined(objectOutputStream, dateRangeFrom);
        super.writeObjectIfDefined(objectOutputStream, dateRangeTo);
        super.writeObjectIfDefined(objectOutputStream, event);
    }

    /**
     * Needed for object serialization (recieving part).
     * @param objectInputStream Specifies the input stream to use.
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void readObject(ObjectInputStream objectInputStream)
    throws IOException, ClassNotFoundException {

        iAction = objectInputStream.readInt();
        iNumEventsToRequest = objectInputStream.readInt();
        sPassphrase = (String) objectInputStream.readObject();
        setMainRegexp(objectInputStream.readBoolean());
        bCaseInsensitive = objectInputStream.readBoolean();

        if (objectInputStream.readBoolean())
            sString = (String) objectInputStream.readObject();

        if (objectInputStream.readBoolean())
            patAll = (Pattern) objectInputStream.readObject();

        if (objectInputStream.readBoolean())
            patCategory = (Pattern) objectInputStream.readObject();

        if (objectInputStream.readBoolean())
            patDescription = (Pattern) objectInputStream.readObject();

        if (objectInputStream.readBoolean())
            patPlace = (Pattern) objectInputStream.readObject();

        if (objectInputStream.readBoolean())
            patDate = (Pattern) objectInputStream.readObject();

        if (objectInputStream.readBoolean())
            dateRangeFrom = (Date) objectInputStream.readObject();

        if (objectInputStream.readBoolean())
            dateRangeTo = (Date) objectInputStream.readObject();

        if (objectInputStream.readBoolean())
            event = (CalendarEvent) objectInputStream.readObject();
    }

    /**
     * Checks if a given pattern string has other charactars than whitespaces inside or not.
     * @param sRegexp Specifies the pattern string to check against.
     * @return Returns true if the pattern string only contains whitespaces. Else, false will be returned.
     */
    private boolean isEmptyPattern(String sRegexp) {
        return !emptyPattern.matcher(sRegexp).find();
    }

    /**
     * This method checks if the request's passphrase is correct or not.
     * @param sPassphrase Specifies the server passphrase to chack against the request passphrase.
     * @return Returns true if the passphrases equal. Else, false will be returned.
     */
    public boolean checkPassphrase(String sPassphrase) {
        return this.sPassphrase.equals(sPassphrase);
    }

    /**
     * This method returns a representation the object as a string.
     * @return Returns the object represented as a string.
     */
    public String toString() {
        return
            "Action        : " + iAction + "\n" +
            "NumEvents     : " + iNumEventsToRequest + "\n" +
            "MainRegexp    : " + bMainRegexp + "\n" +
            "patAll        : " + patAll + "\n" +
            "patCategory   : " + patCategory + "\n" +
            "patDescription: " + patDescription + "\n" +
            "patDate       : " + patDate + "\n" +
            "dateRangeFrom : " + dateRangeFrom + "\n" +
            "dateRangeTo   : " + dateRangeTo + "\n" +
            "modifiedEvent :\n" +  event+ "\n";
    }
}
