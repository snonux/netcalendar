PASSWORD=123456
all:
	@echo Use ant for building the sources
	@echo This Makefile is only for testing purposes
sslclientrun:
	java \
		-Djavax.net.ssl.keyStore=./SSL/clientKeys \
  		-Djavax.net.ssl.keyStorePassword=$(PASSWORD) \
  		-Djavax.net.ssl.trustStore=./SSL/clientTrust \
  		-Djavax.net.ssl.trustStorePassword=$(PASSWORD) \
		shared/Main --client-only --server-address=localhost

sslserverrun:
	java \
		-Djavax.net.ssl.keyStore=./SSL/serverKeys \
  		-Djavax.net.ssl.keyStorePassword=$(PASSWORD) \
  		-Djavax.net.ssl.trustStore=./SSL/serverTrust \
		-Djavax.net.ssl.trustStorePassword=$(PASSWORD) \
		shared/Main --server-only
astyle:
	find ./ -name '*.java' -exec sh -c 'astyle -s4 {}; rm -f {}.orig' \;
check:
	for f in `find ./ -name '*.[ch]'`; do awk -v f=$$f \
		'{ if (length($$0) > 80) { \
		printf "Max line length reached @ %s:%d => %d\n", \
		f, NR, length($$0) } }' $$f; done
newline:
	@echo 
sslkeygen:
	keytool -genkey -keystore mySrvKeystore -keyalg RSA
