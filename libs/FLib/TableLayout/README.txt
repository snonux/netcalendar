			     TableLayout

This package provides a TableLayout manager for use in Java programs.

This program is free software; you can redistribute it and/or modify
it under the terms of the Artistic License. You should have received a
copy of the Artistic License along with this program (in file
License.txt). If not, a copy is available at

    http://opensource.org/licenses/artistic-license.php

To build it you'll need Ant. I used version 1.6.2. Use

  ant all

This will create build/tablelayout.jar. To create the API
documentation, use

  ant javadoc

To view all the documentation, view doc/index.html.

There is a tutorial in doc/tutorial.html. There is also a sample Java
program that uses the TableLayout class. You can build and run this
program with

  ant runExample1

In addition, there is a tool you can use to practice using the
TableLayout. It's called TableExplorer and follows the approach of a
similar tool written for the GridBagLayout. Build and run this program
with

  ant runTableExplorer
