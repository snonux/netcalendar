			       JWizard

This package provides a Wizard component for use in Java programs.

This program is free software; you can redistribute it and/or modify
it under the terms of the Artistic License. You should have received a
copy of the Artistic License along with this program (in file
License.txt). If not, a copy is available at

    http://opensource.org/licenses/artistic-license.php

To build it you'll need Ant. I used version 1.6.2. Use

  ant all

This will create build/jwizard.jar. To create the API documentation, use

  ant javadoc

To view all the documentation, view doc/index.html.

There is a tutorial in doc/tutorial.html. There is also a sample Java
program that uses the JWizard package. You can build and run this
program with

  ant runExample1
