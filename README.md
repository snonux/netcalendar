Quick howto for the NetCalendar 
===============================

![Title](images/netcal.png "NetCalendar")

```
I. HOW TO START THE NETCALENDAR STAND ALONE (WITHOUT SSL):
II. HOW TO START THE NETCALENDAR DISTRIBUTED (WITHOUT SSL):
III. HOW TO START THE NETCALENDAR DISTRIBUTED (WITH SSL):
IV. MORE OPTIONS
V. INFORMATIONS CONCERNING THE COLORS BEING USED WITHIN THE NETCALENDAR CLIENT:
VI. USING THE UNIX CALENDAR DATABASE
```

## I. HOW TO START THE NETCALENDAR STAND ALONE (WITHOUT SSL):

You need at least a JRE 6 or higher. The command

```
java -jar NetCalendar.jar --client-and-server
```

![ScreenShot](images/screenshot.png "Yet another screenshot")

is starting the client and the server of the NetCalendar within the same 
process as two different threads. The option "server_address" will be set
automatically to "localhost". The communication between both threads is done
per TCP/IP.

## II. HOW TO START THE NETCALENDAR DISTRIBUTED (WITHOUT SSL):

1. Server and client should use, if possible, the same version of the JRE.
2. You should copy the complete NetCalendar suite to the client and the server
computer.
3. Start the server with `java -jar NetCalendar.jar --server-only`
4. Start the client with `java -jar NetCalendar.jar --client-only --server-address=$SERVERHOST`

In order to stop the server, select "Server -> Shutdown server" in the GUI of
the client. 

## III. HOW TO START THE NETCALENDAR DISTRIBUTED (WITH SSL):

1. Server and client should use, if possible, the same version of the JRE.
2. You should copy the complete NetCalendar suite to the client and the server
computer. On edit on both sides (server and client) the netcalendar.conf and 
set use_ssl to true.
3. You should create clientKeys,clientTrust,serverKeys and serverTrust as 
discribed in http://www.ibm.com/developerworks/java/library/j-customssl/ and
save them for example into ./SSL. 
3. Start the server with
```
	java \
	-Djavax.net.ssl.keyStore=./SSL/serverKeys \
	-Djavax.net.ssl.keyStorePassword=$PASSWORD \
	-Djavax.net.ssl.trustStore=./SSL/serverTrust \
	-Djavax.net.ssl.trustStorePassword=$PASSWORD \
	-jar NetCalendar.jar --server-only
```
4. Start the client with 
```
	java \
	-Djavax.net.ssl.keyStore=./SSL/clientKeys \
	-Djavax.net.ssl.keyStorePassword=$PASSWORD \
	-Djavax.net.ssl.trustStore=./SSL/clientTrust \
	-Djavax.net.ssl.trustStorePassword=$PASSWORD \
	-jar NetCalendar.jar --client-only --server-address=$SERVERHOST
```

## IV. MORE OPTIONS

All options will be set to their default values while starting the NetCalendar 
without any explicit options given.

If you want to see more available parameters, then run the command

```
java -jar NetCalendar.jar --help
```

It is possible to configure the NetCalendar by editing the configuration file
which is "netcalendar.conf". However, the preferred way is using the client 
GUI via "Session -> Preferences".

## V. INFORMATIONS CONCERNING THE COLORS BEING USED WITHIN THE NETCALENDAR CLIENT:

* Blue in the "Date" column: Yearly event
* Green in the "Date" column: The event will take place only once
* Red: Event takes place within the next 24h
* Orange: Event takes place within the next week
* Yellow: Event takes place within the next 28 days
* Dark: Event takes place within the next 168 days (~ 1/2 year)
* Light beige: Event takes place within the next 365 days (1 year)
* Grey: Event takes place after 365 days

## VI. USING THE UNIX CALENDAR DATABASE

If you are using the `calendar` tool on UNIX, you can use `~/.calendar`` as your
NetCalendar database dir :) 
